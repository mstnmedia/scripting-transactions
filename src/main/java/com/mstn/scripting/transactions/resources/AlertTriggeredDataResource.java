/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.db.common.AlertTriggeredDataService;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a las variables de
 * las alertas disparadas.
 *
 * @author MSTN Media
 */
@Path("/alertTriggeredData")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.ALERTS_LIST})
public class AlertTriggeredDataResource {

	private final AlertTriggeredDataService service;

	/**
	 *
	 * @param service
	 */
	public AlertTriggeredDataResource(AlertTriggeredDataService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Alert_Triggered_Data>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		List<Where> clientWhere = Arrays.asList(
				JSON.toObject(Utils.stringNonNullOrEmpty(where) ? where : "[]", Where[].class)
		);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<Alert_Triggered_Data> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Alert_Triggered_Data>> get(@Auth User user, @PathParam("id") final int id) {
		Alert_Triggered_Data item = service.get(id);
		List<Alert_Triggered_Data> list = Arrays.asList(item);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.ALERTS_ADD})
	public Representation<List<Alert_Triggered_Data>> create(@Auth User user,
			@NotNull @Valid final Alert_Triggered_Data item) {
		Alert_Triggered_Data created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @param id
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.ALERTS_EDIT})
	public Representation<List<Alert_Triggered_Data>> edit(@Auth User user,
			@NotNull @Valid final Alert_Triggered_Data item,
			@PathParam("id") final int id) {
		item.setId(id);
		Alert_Triggered_Data updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.ALERTS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}
}
