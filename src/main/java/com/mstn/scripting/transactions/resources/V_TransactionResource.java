/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.transactions.core.V_TransactionService;
import io.dropwizard.auth.Auth;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja las solicitudes HTTP de consulta de transacciones.
 *
 * @author MSTN Media
 */
@Path("/v_transaction")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
public class V_TransactionResource {

	private final V_TransactionService service;

	/**
	 *
	 * @param V_TransactionService
	 */
	public V_TransactionResource(V_TransactionService V_TransactionService) {
		this.service = V_TransactionService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<V_Transaction>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) throws Exception {
		List<Where> clientWhere = Arrays.asList(JSON.toObject(where, Where[].class));
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		Representation<List<V_Transaction>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws IOException
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<V_Transaction>> getAll(@Auth User user, @QueryParam("where") String where) throws IOException {
		List<Where> clientWhere = Arrays.asList(JSON.toObject(where, Where[].class));
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<V_Transaction> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<V_Transaction>> get(@Auth User user, @PathParam("id") final int id) {
		List<V_Transaction> list = new ArrayList<>();
		V_Transaction transaction = service.get(id, user);

		list.add(transaction);
		return new Representation<>(HttpStatus.OK_200, list);
	}

}
