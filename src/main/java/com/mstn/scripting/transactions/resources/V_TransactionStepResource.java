/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.V_Transaction_Step;
import com.mstn.scripting.transactions.core.V_TransactionStepService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja las solicitudes HTTP de consulta de pasos de transacciones.
 *
 * @author MSTN Media
 */
@Path("/v_transactionstep")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
public class V_TransactionStepResource {

	private final V_TransactionStepService service;

	/**
	 *
	 * @param service
	 */
	public V_TransactionStepResource(V_TransactionStepService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<V_Transaction_Step>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		List<Where> clientWhere = Arrays.asList(
				JSON.toObject(Utils.stringNonNullOrEmpty(where) ? where : "[]", Where[].class)
		);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<V_Transaction_Step> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<V_Transaction_Step>> get(@Auth User user, @PathParam("id") final int id) {
		List<V_Transaction_Step> list = new ArrayList<>();
		list.add(service.get(id));
		return new Representation<>(HttpStatus.OK_200, list);
	}

}
