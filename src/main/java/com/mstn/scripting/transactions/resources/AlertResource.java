/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.transactions.core.AlertService;
import com.mstn.scripting.core.MediaType;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.Alert;
import com.mstn.scripting.core.models.Alert_Result;
import com.mstn.scripting.core.models.Alert_Triggered;
import io.dropwizard.auth.Auth;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a las alertas.
 *
 * @author MSTN Media
 */
@Path("/alert")
@Produces(MediaType.APPLICATION_JSON_UTF_8)
@RolesAllowed({UserPermissions.ALERTS_LIST})
public class AlertResource {

	/**
	 *
	 */
	public final AlertService service;

	/**
	 *
	 * @param service
	 */
	public AlertResource(AlertService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Alert>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) throws Exception {
		WhereClause whereClause = service.getUserWhere(user, Where.listFrom(where));
		Representation<List<Alert>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/getAll")
	public Representation<List<Alert>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		WhereClause whereClause = service.getUserWhere(user, Where.listFrom(where));
		List<Alert> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Alert>> get(@Auth User user, @PathParam("id") final int id) {
		Alert item = service.get(id);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(item));
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 */
	@POST
	@Timed
	@RolesAllowed({UserPermissions.ALERTS_ADD})
	public Representation<List<Alert>> create(@Auth User user, @NotNull @Valid final Alert item) {
		Alert created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param item
	 * @return
	 */
	@PUT
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.ALERTS_EDIT})
	public Representation<List<Alert>> edit(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final Alert item) {
		item.setId(id);
		Alert updated = service.update(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(updated));
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@DELETE
	@Timed
	@Path("/{id}")
	@RolesAllowed({UserPermissions.ALERTS_DELETE})
	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	@GET
	@Timed
	@Path("/results")
	@RolesAllowed({UserPermissions.ALERTS_LIST})
	public Representation<List<Alert_Result>> getResults(@Auth User user) {
		List<Alert_Result> response = service.getResults();
		return new Representation<>(HttpStatus.OK_200, response);
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	@GET
	@Timed
	@Path("/trigger")
	@RolesAllowed({UserPermissions.ALERTS_ADD})
	public Representation<List<Alert_Triggered>> triggerAlerts(@Auth User user) {
		List<Alert_Result> results = service.getResults();
		List<Alert_Triggered> response = service.triggerAlerts(results, user);
		return new Representation<>(HttpStatus.OK_200, response);
	}
}
