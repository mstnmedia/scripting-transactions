package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.Interface_Service;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.transactions.core.V_TransactionService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja solicitudes HTTP relacionadas a transacciones.
 *
 * @author MSTN Media
 */
@Path("/transaction")
@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
public class TransactionResource {

	private final V_TransactionService service;

	/**
	 *
	 * @param vTransactionService
	 */
	public TransactionResource(V_TransactionService vTransactionService) {
		this.service = vTransactionService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public Representation<List<V_Transaction>> getAll(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue(Pagination.DEFAULT_PAGE_SIZE_STR) @QueryParam("pageSize") int pageSize,
			@DefaultValue(Pagination.DEFAULT_PAGE_NUMBER_STR) @QueryParam("pageNumber") int pageNumber,
			@DefaultValue(Pagination.DEFAULT_ORDER_BY) @QueryParam("orderBy") String orderBy
	) throws Exception {
		List<Where> clientWhere = Arrays.asList(
				JSON.toObject(Utils.stringNonNullOrEmpty(where) ? where : "[]", Where[].class)
		);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		Representation<List<V_Transaction>> result = service.getRepresentation(whereClause, pageSize, pageNumber, orderBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 */
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAll")
	public Representation<List<V_Transaction>> getAll(@Auth User user, @QueryParam("where") String where) {
		WhereClause whereClause = new WhereClause(where);
		List<V_Transaction> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Representation<List<V_Transaction>> get(@Auth User user, @PathParam("id") final int id) {
		List<V_Transaction> list = new ArrayList<>();
		V_Transaction transaction = service.get(id, user);
		list.add(transaction);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param item
	 * @return
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> create(@Auth User user, @NotNull @Valid final V_Transaction item) throws Exception {
		V_Transaction created = service.create(item, user);
		return new Representation<>(HttpStatus.OK_200, Arrays.asList(created));
	}

	/**
	 *
	 * @param user
	 * @param id_customer
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@Path("/previous/{id_customer}")
	public List<V_Transaction> getPrevious(@Auth User user, @PathParam("id_customer") final String id_customer) {
		return service.getByCustomer(id_customer);
	}

	/**
	 *
	 * @param user
	 * @param id_customer
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@Path("/previous/{id_customer}")
	public List<V_Transaction> postPrevious(@Auth User user, @PathParam("id_customer") final String id_customer) {
		return getPrevious(user, id_customer);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param params
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@Path("/{id}/updateNote")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public boolean updateNote(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final IdValueParams params) {
		params.setId(id);
		return service.updateNote(params, user);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@Path("/{id}/endCall")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> endCall(
			@Auth User user,
			@PathParam("id") int id,
			@NotNull @Valid IdValueParams params) throws Exception {
		ObjectNode json = JSON.toNode(params.getValue());
		int cause = json.get("cause").asInt();
		String note = json.get("note").asText();
		service.endCall(id, cause, note, user);

		long SR = DATE.nowAsLong();
		Representation<List<V_Transaction>> response = getRunningData(user, Integer.toString(id));
		long ER = DATE.nowAsLong();
		Utils.logInfo(this.getClass(), "ER with " + (ER - SR));

		return response;
	}

	/**
	 *
	 * @param user
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@Path("/{param}/running")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> getRunningData(
			@Auth User user,
			@PathParam("param") final String param) throws Exception {
		List list = null;
		if ("create".equals(param)) {
			list = service.getCreateData(user);
		} else {
			list = service.getRunningData(Integer.parseInt(param), user);
		}
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{param}/running")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> postRunningData(
			@Auth User user,
			@PathParam("param")
			final String param) throws Exception {
		return getRunningData(user, param);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param value
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/sendValue")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> changeCurrentStep(
			@Auth User user,
			@PathParam("id")
			final int id,
			@QueryParam("value") String value) throws Exception {
		List<V_Transaction> list = service.changeCurrentStep(id, value, user);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/sendValue")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> postNextStep(
			@Auth User user,
			@PathParam("id")
			final int id,
			@NotNull
			@Valid
			final IdValueParams params) throws Exception {
		return changeCurrentStep(user, id, params.getValue());
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/sendToInterface")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> sendToInterface(
			@Auth User user,
			@PathParam("id")
			final int id,
			@NotNull
			@Valid
			final IdValueParams params) throws Exception {
		params.setId(id);
		List<V_Transaction> list = service.sendToInterface(params, user);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/goBackStep")
	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
	public Representation<List<V_Transaction>> goBackStep(
			@Auth User user,
			@PathParam("id") final int id,
			@NotNull @Valid final IdValueParams params) throws Exception {
		service.goBackStep(id, Integer.parseInt(params.getValue()), user);
		return getRunningData(user, Integer.toString(id));
	}

	/**
	 *
	 * @param user
	 * @param param
	 * @return
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/usedVersion")
	public long usedVersion(
			@Auth User user,
			@NotNull
			@Valid
			final IdValueParams param) {
		return service.usedVersion(user.getId(), param.getId());
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/consult")
	public Representation<List<V_Transaction>> consult(@Auth User user, @NotNull
			@Valid
			final String where) throws Exception {
		List<Where> clientWhere = Where.listFrom(where);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<V_Transaction> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	@POST
	@Timed
	@Path("/{id}/testVTL")
	@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
	@Produces(MediaType.TEXT_PLAIN)
	public Response postTestVTL(@Auth User user, @PathParam("id") final int id, @FormParam("value") String value) throws Exception {
		return getTestVTL(user, id, value);
	}

	@GET
	@Timed
	@Path("/{id}/testVTL")
	@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
	@Produces(MediaType.TEXT_PLAIN)
	public Response getTestVTL(@Auth User user,
			@PathParam("id") final int id,
			@QueryParam("value") String value) throws Exception {
		String textResult = "";
		try {
			V_Transaction transaction = service.get(id, true, true, user);
			List<Transaction_Result> transactionResults = transaction.getTransaction_results();
			//List<Transaction_Result> transactionSteps = transaction.getTransaction_results();
			Map<String, Object> values = Utils.getMapOf();
			for (Transaction_Result result : transactionResults) {
				values.put(result.getName(), Utils.coalesce(result.getValue(), Transaction_Result.NULL_VALUE));
			}
			values.put("transaction", transaction);
			values.put("transactionResults", transactionResults);
			Workflow_Step step = null;
			try {
				step = Workflow_Step.getStep(
						transaction.getId_current_step(), true,
						service.getHttpClient(),
						service.getConfig().getApiURL(),
						user.getToken()
				);
				values.put("step", step);
			} catch (Exception ex) {
				textResult += "Ha ocurrido un error obteniendo el paso: " + ex.getMessage() + ".\n";
			}
			try {
				Interface inter = Interface.getInterface(
						step.getId_workflow_external(),
						service.getHttpClient(),
						service.getConfig().getApiURL(),
						user.getToken()
				);
				values.put("inter", inter);
			} catch (Exception ex) {
				textResult += "Ha ocurrido un error obteniendo la interfaz: " + ex.getMessage() + ".\n";
			}
			try {
				Interface_Service interService = Interface_Service.getInterfaceService(
						step.getId_interface_service(),
						service.getHttpClient(),
						service.getConfig().getApiURL(),
						user.getToken()
				);
				values.put("service", interService);
			} catch (Exception ex) {
				textResult += "Ha ocurrido un error obteniendo el servicio de la interfaz: " + ex.getMessage() + ".\n";
			}
			textResult += "\n\n";
			String localValue = Utils.coalesce((String) value, "");
			localValue = Velocity.interpolate(localValue, values);
			textResult += "Resultado: \n" + localValue;
		} catch (Exception ex) {
			textResult += ex.getMessage() + "\n" + ex.getLocalizedMessage();
		}
		return Response
				.status(200)
				.entity(textResult)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE")
				//.header("Access-Control-Allow-Headers", returnMethod)
				.build();
	}
}
