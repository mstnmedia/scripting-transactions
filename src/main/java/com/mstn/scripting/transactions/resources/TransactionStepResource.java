/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Transaction_Step;
import com.mstn.scripting.transactions.core.TransactionStepService;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.security.RolesAllowed;
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
//import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.http.HttpStatus;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas a los pasos de las transacciones.
 *
 * @author MSTN Media
 */
@Path("/transactionstep")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.TRANSACTIONS_LIST})
public class TransactionStepResource {

	private final TransactionStepService service;

	/**
	 *
	 * @param service
	 */
	public TransactionStepResource(TransactionStepService service) {
		this.service = service;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	public Representation<List<Transaction_Step>> getAll(@Auth User user, @QueryParam("where") String where) throws Exception {
		List<Where> clientWhere = Arrays.asList(
				JSON.toObject(Utils.stringNonNullOrEmpty(where) ? where : "[]", Where[].class)
		);
		WhereClause whereClause = service.getUserWhere(user, clientWhere);
		List<Transaction_Step> list = service.getAll(whereClause);
		return new Representation<>(HttpStatus.OK_200, list);
	}

	/**
	 *
	 * @param user
	 * @param id
	 * @return
	 */
	@GET
	@Timed
	@Path("/{id}")
	public Representation<List<Transaction_Step>> get(@Auth User user, @PathParam("id") final int id) {
		List<Transaction_Step> list = new ArrayList<>();
		list.add(service.get(id));
		return new Representation<>(HttpStatus.OK_200, list);
	}

//	@POST
//	@Timed
//	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
//	public Representation<List<Transaction_Step>> create(@Auth User user, @NotNull @Valid final Transaction_Step item) {
//		List<Transaction_Step> list = new ArrayList<>();
//		list.add(service.create(item, user));
//		return new Representation<>(HttpStatus.OK_200, list);
//	}
//
//	@PUT
//	@Timed
//	@Path("/{id}")
//	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
//	public Representation<List<Transaction_Step>> edit(@Auth User user, @NotNull @Valid final Transaction_Step item,
//			@PathParam("id") final int id) {
//		item.setId(id);
//
//		List<Transaction_Step> list = new ArrayList<>();
//		list.add(service.update(item, user));
//		return new Representation<>(HttpStatus.OK_200, list);
//	}
//
//	@DELETE
//	@Timed
//	@Path("/{id}")
//	@RolesAllowed({UserPermissions.TRANSACTIONS_ADD})
//	public Representation<String> delete(@Auth User user, @PathParam("id") final int id) {
//		return new Representation<>(HttpStatus.OK_200, service.delete(id, user));
//	}
}
