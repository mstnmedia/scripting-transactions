/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.resources;

import com.codahale.metrics.annotation.Timed;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.auth.jwt.UserPermissions;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.transactions.core.DashboardNPSService;
import com.mstn.scripting.transactions.core.DashboardService;
import com.mstn.scripting.transactions.db.dashboard.DashboardRepresentation;
import io.dropwizard.auth.Auth;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

/**
 * Clase que maneja todas las solicitudes HTTP relacionadas al Dashboard.
 *
 * @author MSTN Media
 */
@Path("/dashboard")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({UserPermissions.DASHBOARD_SHOW})
public class DashboardResource {

	private final DashboardService service;
	private final DashboardNPSService npsService;

	/**
	 *
	 * @param service
	 * @param npsService
	 */
	public DashboardResource(DashboardService service, DashboardNPSService npsService) {
		this.service = service;
		this.npsService = npsService;
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param type1
	 * @param type2
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/byProductType")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation getByProductType(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2
	) throws Exception {
		List<Where> filters = Where.listFrom(where);
		return postByProductType(user, filters, type1, type2);
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/byProductType")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation postByProductType(@Auth User user,
			@Valid final List<Where> filters,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2
	) throws Exception {
		try {
			DashboardRepresentation result = service.byProductType(user, filters, type1, type2);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param type1
	 * @param type2
	 * @param groupBy
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/historyByDay")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation getHistoryByDay(@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2,
			@DefaultValue("") @QueryParam("groupBy") String groupBy
	) throws Exception {
		List<Where> filters = Where.listFrom(where);
		return postHistoryByDay(user, filters, type1, type2, groupBy);
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @param groupBy
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/historyByDay")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation postHistoryByDay(@Auth User user,
			@NotNull @Valid final List<Where> filters,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2,
			@DefaultValue("") @QueryParam("groupBy") String groupBy
	) throws Exception {
		try {
			DashboardRepresentation result = service.historyByDay(user, filters, type1, type2, groupBy);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @param type1
	 * @param type2
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/byCategory")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation getByCategory(@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2) throws Exception {
		List<Where> filters = Where.listFrom(where);
		return postByCategory(user, filters, type1, type2);
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/byCategory")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS1})
	public DashboardRepresentation postByCategory(@Auth User user, @NotNull @Valid final List<Where> filters,
			@DefaultValue("") @QueryParam("type1") String type1,
			@DefaultValue("") @QueryParam("type2") String type2) throws Exception {
		try {
			DashboardRepresentation result = service.byCategory(user, filters, type1, type2);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/timeAverage")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS2})
	public DashboardRepresentation getTimeAverage(
			@Auth User user,
			@DefaultValue(Pagination.DEFAULT_WHERE) @QueryParam("where") String where
	) throws Exception {
		List<Where> filters = Where.listFrom(where);
		return postTimeAverage(user, filters);
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/timeAverage")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS2})
	public DashboardRepresentation postTimeAverage(
			@Auth User user,
			@NotNull @Valid final List<Where> filters
	) throws Exception {
		try {
			DashboardRepresentation result = service.timeAverage(user, filters);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param user
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	@GET
	@Timed
	@Path("/nps")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS2})
	public DashboardRepresentation getNPS(@Auth User user,
			@QueryParam("from") String from,
			@QueryParam("to") String to) throws Exception {
		return postNPS(user, from, to);
	}

	/**
	 *
	 * @param user
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	@POST
	@Timed
	@Path("/nps")
	@RolesAllowed({UserPermissions.DASHBOARD_CHARTS2})
	public DashboardRepresentation postNPS(@Auth User user,
			@QueryParam("from") String from,
			@QueryParam("to") String to) throws Exception {
		try {
			DashboardRepresentation result = npsService.resultadoNPS(user, from, to);
			return result;
		} catch (Exception ex) {
			throw new WebApplicationException(ex.getMessage(), ex);
		}
	}
}
