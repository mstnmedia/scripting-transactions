package com.mstn.scripting.transactions;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.AuthFilterUtils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.common.DelegationService;
import com.mstn.scripting.core.db.common.EmployeeService;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Log;
import com.mstn.scripting.transactions.core.AlertColumnService;
import com.mstn.scripting.transactions.core.AlertCriteriaService;
import com.mstn.scripting.transactions.core.AlertService;
import com.mstn.scripting.core.db.common.AlertTriggeredDataService;
import com.mstn.scripting.core.db.common.AlertTriggeredService;
import com.mstn.scripting.core.db.common.AlertTriggeredTransactionService;
import com.mstn.scripting.transactions.core.CategoryService;
import com.mstn.scripting.transactions.core.DashboardNPSService;
import com.mstn.scripting.transactions.core.DashboardService;
import com.mstn.scripting.transactions.core.EndCallService;
import com.mstn.scripting.transactions.core.TransactionResultService;
import com.mstn.scripting.transactions.core.TransactionService;
import com.mstn.scripting.transactions.core.TransactionStepService;
import com.mstn.scripting.transactions.core.V_TransactionService;
import com.mstn.scripting.transactions.core.V_TransactionStepService;
import com.mstn.scripting.transactions.health.ScriptingTransactionsHealthCheck;
import com.mstn.scripting.transactions.resources.AlertColumnResource;
import com.mstn.scripting.transactions.resources.AlertCriteriaResource;
import com.mstn.scripting.transactions.resources.AlertResource;
import com.mstn.scripting.transactions.resources.AlertTriggeredDataResource;
import com.mstn.scripting.transactions.resources.AlertTriggeredResource;
import com.mstn.scripting.transactions.resources.CategoryResource;
import com.mstn.scripting.transactions.resources.DashboardResource;
import com.mstn.scripting.transactions.resources.EndCallResource;
import com.mstn.scripting.transactions.resources.TransactionResource;
import com.mstn.scripting.transactions.resources.V_TransactionResource;
import com.mstn.scripting.transactions.resources.V_TransactionStepResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.PolymorphicAuthDynamicFeature;
import io.dropwizard.auth.PolymorphicAuthValueFactoryProvider;
import io.dropwizard.auth.PrincipalImpl;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.bundles.DBIExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.HttpClient;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtContext;
import org.simplejavamail.mailer.Mailer;
import org.skife.jdbi.v2.DBI;

/**
 * Clase ejecutable que inicia el servidor y registra los componentes necesarios
 * para el funcionamiento del microservicio Transactions.
 *
 * @author MSTN Media
 */
public class ScriptingTransactionsApplication extends Application<ScriptingTransactionsConfiguration> {

	private static final String TRANSACTION_SERVICE = "Transaction service";

	/**
	 *
	 */
	public static EmployeeService EmployeeService;

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		new ScriptingTransactionsApplication().run(args);
	}

	@Override
	public String getName() {
		return "ScriptingTransactions";
	}

	@Override
	public void initialize(final Bootstrap<ScriptingTransactionsConfiguration> bootstrap) {
		bootstrap.addBundle(new DBIExceptionsBundle());
	}

	@Override
	public void run(final ScriptingTransactionsConfiguration config, final Environment environment) {
		TimeZone.setDefault(TimeZone.getTimeZone(config.getTimeZone()));
		Locale.setDefault(Locale.forLanguageTag(config.getLocale()));
		registerResources(config, environment);
		registerAuthFilters(environment);
	}

	private void registerResources(ScriptingTransactionsConfiguration config, Environment environment) {
		final DBIFactory factory = new DBIFactory();
		final DBI dbi = factory.build(environment, config.getDataSourceFactory(), "database");
		final Mailer mailer = config.getMailServer().build(environment, "mailer");

		InterfaceConfigs.mailer = mailer;
		InterfaceConfigs.configs.put("mailFromName", config.getMailFromName());
		InterfaceConfigs.configs.put("mailFromEmail", config.getMailFromEmail());
		InterfaceConfigs.configs.put("npsConnectionString", config.getNPSConnectionString());
		final HttpClient httpClient = new HttpClientBuilder(environment)
				.using(config.getHttpClientConfiguration())
				.build(getName());
		AlertTriggeredDataService alertTriggeredDataService = dbi.onDemand(AlertTriggeredDataService.class);
		AlertTriggeredTransactionService alertTriggeredTransactionService = dbi.onDemand(AlertTriggeredTransactionService.class);
		AlertTriggeredService alertTriggeredService = dbi.onDemand(AlertTriggeredService.class)
				.setDataService(alertTriggeredDataService)
				.setTransactionService(alertTriggeredTransactionService);
		AlertColumnService alertColumnService = dbi.onDemand(AlertColumnService.class);
		AlertCriteriaService alertCriteriaService = dbi.onDemand(AlertCriteriaService.class);
		AlertService alertService = dbi.onDemand(AlertService.class)
				.setColumnService(alertColumnService)
				.setCriteriaService(alertCriteriaService)
				.setTriggeredService(alertTriggeredService);

		DelegationService delegationService = dbi.onDemand(DelegationService.class);
		ScriptingTransactionsApplication.EmployeeService = dbi.onDemand(EmployeeService.class)
				.setDelegationService(delegationService);
		CategoryService categoryService = dbi.onDemand(CategoryService.class);
		EndCallService endCallService = dbi.onDemand(EndCallService.class);
		DashboardService dashboardService = dbi.onDemand(DashboardService.class);
		DashboardNPSService dashboardNPSService = new DashboardNPSService();

		TransactionStepService transactionStepService = dbi.onDemand(TransactionStepService.class);
		V_TransactionStepService vTransactionStepService = dbi.onDemand(V_TransactionStepService.class);
		TransactionResultService transactionResultService = dbi.onDemand(TransactionResultService.class);
		TransactionService transactionService = (dbi.onDemand(TransactionService.class))
				.setConfig(config)
				.setHttpClient(httpClient)
				.setTransactionStepService(transactionStepService)
				.setV_TransactionStepService(vTransactionStepService)
				.setTransactionResultService(transactionResultService);
		V_TransactionService vTransactionService = dbi.onDemand(V_TransactionService.class)
				.setConfig(config)
				.setHttpClient(httpClient)
				.setAlertsService(alertTriggeredService, alertTriggeredTransactionService)
				.setTransactionStepService(transactionStepService)
				.setV_TransactionStepService(vTransactionStepService)
				.setTransactionResultService(transactionResultService)
				.setCategoryService(categoryService);

		TransactionResource transactionResource = new TransactionResource(vTransactionService);

		ScriptingTransactionsHealthCheck healthCheck = new ScriptingTransactionsHealthCheck(transactionService);
		environment.healthChecks().register(TRANSACTION_SERVICE, healthCheck);
		AlertResource alertResource = new AlertResource(alertService);
		environment.jersey().register(transactionResource);
		environment.jersey().register(alertResource);
		environment.jersey().register(new AlertColumnResource(alertColumnService));
		environment.jersey().register(new AlertCriteriaResource(alertCriteriaService));
		environment.jersey().register(new AlertTriggeredResource(alertTriggeredService));
		environment.jersey().register(new AlertTriggeredDataResource(alertTriggeredDataService));
		environment.jersey().register(new V_TransactionResource(vTransactionService));
		environment.jersey().register(new V_TransactionStepResource(vTransactionStepService));
		environment.jersey().register(new CategoryResource(categoryService));
		environment.jersey().register(new EndCallResource(endCallService));
		environment.jersey().register(new DashboardResource(dashboardService, dashboardNPSService));

		proccessAlerts(config, alertResource);
	}

	private void proccessAlerts(ScriptingTransactionsConfiguration config, AlertResource alertResource) {
		long minutesForProccessAlerts = config.getMinutesForProccessAlerts();
		if (0 < minutesForProccessAlerts) {
			Runnable processAlerts = () -> {
				try {
					alertResource.triggerAlerts(User.SYSTEM);
					Utils.logDebug(this.getClass(), "Proceso de alertas ejecutado correctamente");
				} catch (Exception ex) {
					try {
						Utils.logException(this.getClass(), "Error invoking AlertResource.triggerAlerts with user User.SYSTEM", ex);
						alertResource.service.log().insert(new Log(
								0, User.SYSTEM.getId(),
								"Proceso de alertas", 0,
								"Error", new Date(),
								ex.getMessage(), JSON.toString(ex),
								User.SYSTEM.getIp()
						));
					} catch (Exception exLog) {
						Utils.logException(this.getClass(), "Error while inserting error log in database", exLog);
					}
				}
			};
			ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
			exec.scheduleAtFixedRate(processAlerts, 0, minutesForProccessAlerts, TimeUnit.MINUTES);
		}
	}

	/**
	 * Registers the filters that will handle authentication
	 */
	private void registerAuthFilters(Environment environment) {
		AuthFilterUtils authFilterUtils = new AuthFilterUtils();
		final AuthFilter<BasicCredentials, PrincipalImpl> basicFilter = authFilterUtils.buildBasicAuthFilter();
		final AuthFilter<JwtContext, User> jwtFilter = authFilterUtils.buildJwtAuthFilter();

		final PolymorphicAuthDynamicFeature feature = new PolymorphicAuthDynamicFeature<>(
				ImmutableMap.of(PrincipalImpl.class, basicFilter, User.class, jwtFilter));
		final AbstractBinder binder = new PolymorphicAuthValueFactoryProvider.Binder<>(
				ImmutableSet.of(PrincipalImpl.class, User.class));

		environment.jersey().register(feature);
		environment.jersey().register(binder);
		environment.jersey().register(RolesAllowedDynamicFeature.class);
	}
}
