/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.health;

import com.codahale.metrics.health.HealthCheck;
import com.mstn.scripting.transactions.core.TransactionService;

/**
 * Clase que se registra para consultar el estado del microservicio.
 *
 * @author amatos
 */
public class ScriptingTransactionsHealthCheck extends HealthCheck {

	private static final String HEALTHY = "The Workflow Service is healthy for read and write";
	private static final String UNHEALTHY = "The Workflow Service is not healthy. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";

	private final TransactionService transactionService;

	/**
	 *
	 * @param transactionService
	 */
	public ScriptingTransactionsHealthCheck(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@Override
	protected Result check() throws Exception {
		String dbHealthStatus = transactionService.performHealthCheck();

		if (dbHealthStatus == null) {
			return Result.healthy(HEALTHY);
		} else {
			return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, dbHealthStatus);
		}
	}
}
