package com.mstn.scripting.transactions;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.mail.MailerFactory;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define la configuración del microservicio Transactions.
 *
 * @author MSTN Media
 */
public class ScriptingTransactionsConfiguration extends Configuration {

	private static final String DATABASE = "database";
	private static final String NPS_CONNECTION_STRING = "npsConnectionString";
	private static final String API_URL = "apiURL";
	private static final String HTTP_CLIENT = "httpClient";
	private static final String TIME_ZONE = "timeZone";
	private static final String MINUTES_FOR_PROCCESS_ALERTS = "minutesForProccessAlerts";
	private static final String MAIL_FROM_NAME = "mailFromName";
	private static final String MAIL_FROM_EMAIL = "mailFromEmail";
	private static final String MAIL_SERVER = "mailServer";

	@NotEmpty
	private String apiURL;

	@NotEmpty
	private String timeZone;

	@NotNull
	private long minutesForProccessAlerts;

	@NotEmpty
	private String mailFromName;

	@NotEmpty
	private String mailFromEmail;

	@Valid
	@NotNull
	private MailerFactory mailServer = new MailerFactory();

	@Valid
	@NotNull
	private DataSourceFactory dataSourceFactory = new DataSourceFactory();

	@NotEmpty
	private String npsConnectionString;

	@Valid
	@NotNull
	private HttpClientConfiguration httpClient = new HttpClientConfiguration();

	/**
	 *
	 * @return
	 */
	@JsonProperty(DATABASE)
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	/**
	 *
	 * @param dataSourceFactory
	 */
	@JsonProperty(DATABASE)
	public void setDataSourceFactory(final DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(NPS_CONNECTION_STRING)
	public String getNPSConnectionString() {
		return npsConnectionString;
	}

	/**
	 *
	 * @param npsConnectionString
	 */
	@JsonProperty(NPS_CONNECTION_STRING)
	public void setNPSConnectionString(final String npsConnectionString) {
		this.npsConnectionString = npsConnectionString;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(API_URL)
	public String getApiURL() {
		return apiURL;
	}

	/**
	 *
	 * @param apiURL
	 */
	@JsonProperty(API_URL)
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(HTTP_CLIENT)
	public HttpClientConfiguration getHttpClientConfiguration() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 */
	@JsonProperty(HTTP_CLIENT)
	public void setHttpClientConfiguration(HttpClientConfiguration httpClient) {
		this.httpClient = httpClient;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(TIME_ZONE)
	public String getTimeZone() {
		return timeZone;
	}

	@NotEmpty
	private String locale;

	/**
	 *
	 * @return
	 */
	@JsonProperty("locale")
	public String getLocale() {
		return locale;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MINUTES_FOR_PROCCESS_ALERTS)
	public long getMinutesForProccessAlerts() {
		return minutesForProccessAlerts;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_FROM_NAME)
	public String getMailFromName() {
		return mailFromName;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_FROM_EMAIL)
	public String getMailFromEmail() {
		return mailFromEmail;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_SERVER)
	public MailerFactory getMailServer() {
		return mailServer;
	}
}
