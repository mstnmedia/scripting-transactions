/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.ClobValue;
import com.mstn.scripting.core.db.ClobValueBinder;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import static com.mstn.scripting.core.db.common.EmployeeDao.TREEDOWNBASE;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.Pagination;
import com.mstn.scripting.core.models.V_Transaction;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la vista {@code v_transaction}.
 *
 * @author amatos
 */
@RegisterMapper(V_TransactionDao.V_TransactionMapper.class)
@UseStringTemplate3StatementLocator
public abstract class V_TransactionDao {

	/**
	 *
	 */
	static public class V_TransactionMapper implements ResultSetMapper<V_Transaction> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String CENTER_NAME = "center_name";
		private static final String ID_CALL = "id_call";
		private static final String ID_CATEGORY = "id_category";
		private static final String ID_INDICATOR = "id_indicator";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_VERSION = "id_workflow_version";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String ID_CUSTOMER = "id_customer";
		private static final String ID_SUBSCRIPTION = "id_subscription";
		private static final String ID_CURRENT_STEP = "id_current_step";
		private static final String START_DATE = "start_date";
		private static final String END_DATE = "end_date";
		private static final String END_CAUSE = "end_cause";
		private static final String END_ID_EMPLOYEE = "end_id_employee";
		private static final String STATE = "state";
		private static final String SUBSCRIBER_NO = "subscriber_no";
		private static final String CALLER_ALT_PHONE = "caller_alt_phone";
		private static final String NOTE = "note";
		private static final String WORKFLOW_NAME = "workflow_name";
		private static final String VERSION_ID_VERSION = "version_id_version";
		private static final String WORKFLOW_STEP_NAME = "workflow_step_name";
		private static final String WORKFLOW_STEP_ENTRY_DATE = "workflow_step_entry_date";
		private static final String EMPLOYEE_NAME = "employee_name";
		private static final String CATEGORY_NAME = "category_name";
		private static final String END_CAUSE_NAME = "end_cause_name";
		private static final String END_EMPLOYEE_NAME = "end_employee_name";

		@Override
		public V_Transaction map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new V_Transaction(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(ID_CALL) ? resultSet.getString(ID_CALL) : "",
					hasColumn.containsKey(ID_CATEGORY) ? resultSet.getInt(ID_CATEGORY) : 0,
					hasColumn.containsKey(ID_INDICATOR) ? resultSet.getInt(ID_INDICATOR) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_VERSION) ? resultSet.getInt(ID_WORKFLOW_VERSION) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(ID_CUSTOMER) ? resultSet.getString(ID_CUSTOMER) : "",
					hasColumn.containsKey(ID_SUBSCRIPTION) ? resultSet.getString(ID_SUBSCRIPTION) : "",
					hasColumn.containsKey(ID_CURRENT_STEP) ? resultSet.getInt(ID_CURRENT_STEP) : 0,
					hasColumn.containsKey(START_DATE) ? resultSet.getDate(START_DATE) : null,
					hasColumn.containsKey(END_DATE) ? resultSet.getDate(END_DATE) : null,
					hasColumn.containsKey(END_CAUSE) ? resultSet.getInt(END_CAUSE) : 0,
					hasColumn.containsKey(END_ID_EMPLOYEE) ? resultSet.getInt(END_ID_EMPLOYEE) : 0,
					hasColumn.containsKey(STATE) ? resultSet.getInt(STATE) : 0,
					hasColumn.containsKey(SUBSCRIBER_NO) ? resultSet.getString(SUBSCRIBER_NO) : "",
					hasColumn.containsKey(CALLER_ALT_PHONE) ? resultSet.getString(CALLER_ALT_PHONE) : "",
					hasColumn.containsKey(NOTE) ? resultSet.getString(NOTE) : "",
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : "",
					hasColumn.containsKey(VERSION_ID_VERSION) ? resultSet.getString(VERSION_ID_VERSION) : "",
					hasColumn.containsKey(WORKFLOW_STEP_NAME) ? resultSet.getString(WORKFLOW_STEP_NAME) : "",
					hasColumn.containsKey(WORKFLOW_STEP_ENTRY_DATE) ? resultSet.getDate(WORKFLOW_STEP_ENTRY_DATE) : null,
					hasColumn.containsKey(EMPLOYEE_NAME) ? resultSet.getString(EMPLOYEE_NAME) : "",
					hasColumn.containsKey(CATEGORY_NAME) ? resultSet.getString(CATEGORY_NAME) : "",
					hasColumn.containsKey(END_CAUSE_NAME) ? resultSet.getString(END_CAUSE_NAME) : "",
					hasColumn.containsKey(END_EMPLOYEE_NAME) ? resultSet.getString(END_EMPLOYEE_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_transaction <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @return
	 */
	public List<V_Transaction> getAll() {
		return getAll(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where) {
		return getAll(where, Pagination.DEFAULT_PAGE_SIZE);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where, int pageSize) {
		return getAll(where, pageSize, Pagination.DEFAULT_PAGE_NUMBER);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where, int pageSize, int pageNumber) {
		return getAll(where, pageSize, pageNumber, Pagination.DEFAULT_ORDER_BY);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, t.* from ("
			+ "  select t.* from v_transaction t <where> order by <order_by>"
			+ " ) t"
			+ ") t where t.rowno >= ((:page_number - 1) * :page_size + 1) and t.rowno \\<= (:page_number * :page_size)")
	abstract public List<V_Transaction> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	// TODO: order_by debe ser una lista como WhereClause
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_transaction <where> order by ID desc")
	abstract public List<V_Transaction> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_customer
	 * @return
	 */
	@SqlQuery("select * from v_transaction where id_customer = :id_customer order by start_date desc")
	abstract public List<V_Transaction> getByCustomer(@Bind("id_customer") final String id_customer);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_transaction where id = :id")
	abstract public V_Transaction get(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @param userID
	 * @return
	 */
	@SqlQuery(TREEDOWNBASE + "select * from v_transaction where id = :id and id_employee in (select id from treeDown)")
	abstract public V_Transaction getForUser(@Bind("id") final int id, @Bind("userID") final long userID);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into transaction(id, id_center, id_call, id_category, id_indicator, id_workflow, id_workflow_version, id_employee, id_customer, id_subscription, id_current_step, start_date, end_date, end_cause, end_id_employee, state, subscriber_no, caller_alt_phone, note) "
			+ "values(transaction_seq.nextVal, :id_center, :id_call, :id_category, :id_indicator, :id_workflow, :id_workflow_version, :id_employee, :id_customer, :id_subscription, :id_current_step, :start_date, :end_date, :end_cause, :end_id_employee, :state, :subscriber_no, :caller_alt_phone, :note)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final V_Transaction item);

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update transaction set "
			+ " id_center = :id_center, "
			+ " id_call = :id_call, "
			+ " id_category = :id_category, "
			+ " id_indicator = :id_indicator, "
			+ " id_workflow = :id_workflow, "
			+ " id_workflow_version = :id_workflow_version, "
			+ " id_employee = :id_employee, "
			+ " id_customer = :id_customer, "
			+ " id_subscription = :id_subscription, "
			+ " id_current_step = :id_current_step, "
			+ " start_date = :start_date, "
			+ " end_date = :end_date, "
			+ " end_cause = :end_cause, "
			+ " end_id_employee = :end_id_employee, "
			+ " state = :state, "
			+ " subscriber_no = :subscriber_no, "
			+ " caller_alt_phone = :caller_alt_phone, "
			+ " note = <value> "
			+ "where id = :id")
	abstract public void update(@BindBean final V_Transaction item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 */
	public void update(V_Transaction item) {
		ClobValue value = new ClobValue(item.getNote());
		update(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param id_transaction
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update transaction set note = <value> where id = :id_transaction")
	abstract public void updateNote(
			@Bind("id_transaction") final int id_transaction,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param id_transaction
	 * @param pNote
	 */
	public void updateNote(int id_transaction, String pNote) {
		ClobValue value = new ClobValue(pNote);
		updateNote(id_transaction, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param id_transaction
	 * @param name
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update transaction_result set value = <value> where id_transaction=:id_transaction and name=:name")
	abstract public void updateNoteVariable(
			@Bind("id_transaction") final int id_transaction,
			@Bind("name") final String name,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param id_transaction
	 * @param pNote
	 */
	public void updateNoteVariable(int id_transaction, String pNote) {
		ClobValue value = new ClobValue(pNote);
		String name = Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_note";
		updateNoteVariable(id_transaction, name, value.getPreparedString(), value);
	}

}
