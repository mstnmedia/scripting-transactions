/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db.dashboard;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase que crea una instancia de {@link DashboardItem} desde un
 * {@link ResultSet} que contiene el registro calculado que se enviará a un
 * gráfico del Dashboard.
 *
 * @author amatos
 */
public class DashboardItemMapper implements ResultSetMapper<DashboardItem> {

	private static final String ID = "id";
	private static final String ID_PARENT = "id_parent";
	private static final String LABEL = "label";
	private static final String VALUE = "value";
	private static final String OBJETIVE = "objetive";
	private static final String MARGIN = "margin";
//	private static final String DESVIATION = "DESVIATION";

	@Override
	public DashboardItem map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
		HashMap<String, Boolean> hasColumn = new HashMap<>();
		ResultSetMetaData rsmd = resultSet.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
		}
		return new DashboardItem(
				hasColumn.containsKey(ID) ? resultSet.getLong(ID) : 0,
				hasColumn.containsKey(ID_PARENT) ? resultSet.getLong(ID_PARENT) : 0,
				hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
				hasColumn.containsKey(VALUE) ? resultSet.getLong(VALUE) : 0,
				hasColumn.containsKey(OBJETIVE) ? resultSet.getDouble(OBJETIVE) : 0,
				hasColumn.containsKey(MARGIN) ? resultSet.getDouble(MARGIN) : 0
		);
	}
}
