/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db.dashboard;

/**
 * Clase que envuelve la respuesta de datos enviados a un gráfico del Dashboard.
 *
 * @author amatos
 * @param <T> Clase que define las propiedades de la respuesta.
 */
public class DashboardRepresentation<T> {

	private String range;
	private T items;
	private int pageNumber;

	/**
	 *
	 */
	public DashboardRepresentation() {
	}

	/**
	 *
	 * @param range
	 * @param items
	 */
	public DashboardRepresentation(String range, T items) {
		this.range = range;
		this.items = items;
	}

	/**
	 *
	 * @return
	 */
	public String getRange() {
		return range;
	}

	/**
	 *
	 * @param range
	 */
	public void setRange(String range) {
		this.range = range;
	}

	/**
	 *
	 * @return
	 */
	public T getItems() {
		return items;
	}

	/**
	 *
	 * @param items
	 */
	public void setItems(T items) {
		this.items = items;
	}

	/**
	 *
	 * @return
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 *
	 * @param pageNumber
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

}
