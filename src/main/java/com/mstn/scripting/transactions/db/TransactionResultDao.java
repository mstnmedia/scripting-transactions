/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Transaction_Result;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code transaction_result}.
 *
 * @author amatos
 */
@RegisterMapper(TransactionResultDao.Mapper.class)
@UseStringTemplate3StatementLocator
public abstract class TransactionResultDao {

	/**
	 *
	 */
	static public class Mapper implements ResultSetMapper<Transaction_Result> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_TRANSACTION = "id_transaction";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String ID_INTERFACE = "id_interface";
		private static final String NAME = "name";
		private static final String LABEL = "label";
		private static final String VALUE = "value";
		private static final String PROPS = "props";

		@Override
		public Transaction_Result map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Transaction_Result(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_TRANSACTION) ? resultSet.getInt(ID_TRANSACTION) : 0,
					hasColumn.containsKey(ID_WORKFLOW_STEP) ? resultSet.getInt(ID_WORKFLOW_STEP) : 0,
					hasColumn.containsKey(ID_INTERFACE) ? resultSet.getInt(ID_INTERFACE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(LABEL) ? resultSet.getString(LABEL) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "",
					hasColumn.containsKey(PROPS) ? resultSet.getString(PROPS) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from transaction_result tr <where>")
	abstract public List<Transaction_Result> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_transaction
	 * @return
	 */
	@SqlQuery("select * from transaction_result tr where id_transaction = :id_transaction")
	abstract public List<Transaction_Result> getAllByTransaction(@Bind("id_transaction") final int id_transaction);

	/**
	 *
	 * @param id_transaction
	 * @param id_interface
	 * @return
	 */
	@SqlQuery("select * from transaction_result where id_transaction=:id_transaction and id_interface=:id_interface")
	abstract public List<Transaction_Result> getAllByTransactionFromInterface(@Bind("id_transaction") int id_transaction, @Bind("id_interface") int id_interface);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from transaction_result tr where id = :id")
	abstract public Transaction_Result get(@Bind("id") final int id);

	/**
	 *
	 * @param id_transaction
	 * @param name
	 * @return
	 */
	@SqlQuery("select * from transaction_result tr where id_transaction = :id_transaction and name = :name")
	abstract public Transaction_Result getByName(@Bind("id_transaction") final int id_transaction, @Bind("name") final String name);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into transaction_result(id, id_center, id_transaction, id_workflow_step, id_interface, name, label, value, props) values(transaction_result_seq.nextVal, :id_center, :id_transaction, :id_workflow_step, :id_interface, :name, :label, :value, :props)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Transaction_Result item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update transaction_result set "
			+ " id_center = :id_center, "
			+ " id_transaction = :id_transaction, "
			+ " id_workflow_step = :id_workflow_step, "
			+ " id_interface = :id_interface, "
			+ " name = :name, "
			+ " label = :label, "
			+ " value = to_clob(:value), "
			+ " props = :props "
			+ "where id = :id")
	abstract public void update(@BindBean final Transaction_Result item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from transaction_result where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id_transaction
	 * @param id_interface
	 * @return
	 */
	@SqlUpdate("delete from transaction_result "
			+ "where id_transaction=:id_transaction and id_interface=:id_interface")
	abstract public int delete(@Bind("id_transaction") int id_transaction, @Bind("id_interface") int id_interface);
}
