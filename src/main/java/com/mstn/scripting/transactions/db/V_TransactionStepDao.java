/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.V_Transaction_Step;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la vista {@code v_transaction_step}.
 *
 * @author amatos
 */
@RegisterMapper(V_TransactionStepDao.Mapper.class)
@UseStringTemplate3StatementLocator
public interface V_TransactionStepDao {

	/**
	 *
	 */
	public class Mapper implements ResultSetMapper<V_Transaction_Step> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_TRANSACTION = "id_transaction";
		private static final String ID_PARENT = "id_parent";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String DOCDATE = "docdate";
		private static final String VALUE = "value";
		private static final String ACTIVE = "active";
		private static final String WORKFLOW_NAME = "workflow_name";
		private static final String WORKFLOW_STEP_NAME = "workflow_step_name";
		private static final String EMPLOYEE_NAME = "employee_name";

		@Override
		public V_Transaction_Step map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new V_Transaction_Step(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_TRANSACTION) ? resultSet.getInt(ID_TRANSACTION) : 0,
					hasColumn.containsKey(ID_PARENT) ? resultSet.getInt(ID_PARENT) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_STEP) ? resultSet.getInt(ID_WORKFLOW_STEP) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(DOCDATE) ? resultSet.getDate(DOCDATE) : null,
					hasColumn.containsKey(ACTIVE) ? resultSet.getBoolean(ACTIVE) : false,
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : null,
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : null,
					hasColumn.containsKey(WORKFLOW_STEP_NAME) ? resultSet.getString(WORKFLOW_STEP_NAME) : null,
					hasColumn.containsKey(EMPLOYEE_NAME) ? resultSet.getString(EMPLOYEE_NAME) : null
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_transaction_step <where> order by id")
	List<V_Transaction_Step> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_transaction
	 * @return
	 */
	@SqlQuery("select * from v_transaction_step where id_transaction = :id_transaction order by id")
	List<V_Transaction_Step> getAllByTransaction(@Bind("id_transaction") final int id_transaction);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_transaction_step where id = :id")
	V_Transaction_Step get(@Bind("id") final int id);

	/**
	 *
	 * @param id_transaction
	 * @return
	 */
	@SqlQuery("select * from v_transaction_step where id_transaction = :id_transaction and active = 1")
	V_Transaction_Step getActive(@Bind("id_transaction") final int id_transaction);

}
