/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.enums.AlertTimeUnits;
import com.mstn.scripting.core.models.Alert;
import com.mstn.scripting.core.models.Alert_Column;
import com.mstn.scripting.core.models.Alert_Criteria;
import com.mstn.scripting.core.models.Alert_Result;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase que calcula las alertas que aplican para ser disparadas.
 *
 * @author amatos
 */
@RegisterMapper(AlertResultDao.AlertResultMapper.class)
@UseStringTemplate3StatementLocator
public abstract class AlertResultDao {

	/**
	 *
	 */
	static public class AlertResultMapper implements ResultSetMapper<Alert_Result> {

		private static final String ID_CENTER = "id_center";
		private static final String ID_ALERT = "id_alert";
		private static final String FIRST_TRANSACTION_DATE = "first_transaction_date";
		private static final String COLUMNS = "columns";
		private static final String QUANTITY = "quantity";

		@Override
		public Alert_Result map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert_Result(
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_ALERT) ? resultSet.getInt(ID_ALERT) : 0,
					hasColumn.containsKey(FIRST_TRANSACTION_DATE) ? resultSet.getDate(FIRST_TRANSACTION_DATE) : null,
					hasColumn.containsKey(COLUMNS) ? resultSet.getString(COLUMNS) : "",
					hasColumn.containsKey(QUANTITY) ? resultSet.getInt(QUANTITY) : 0
			);
		}
	}

	@SqlQuery("select id_center, min(first_transaction_date) first_transaction_date, columns, count(*) quantity "
			+ "from ("
			+ "	select id_center, id_transaction, first_transaction_date, listagg(name, ',') within group (order by name) as columns"
			+ "	from ("
			+ "		select t.id_center, tr.id_transaction, "
			+ "			min(t.start_date) first_transaction_date, "
			+ "			tr.name||'='||to_char(tr.value) name, "
			+ "			count(*) quantity"
			+ "		from transaction_result tr"
			+ "			inner join transaction t on t.id=tr.id_transaction and t.id_center=:id_center"
			+ "		<baseWhere>"
			+ "		group by t.id_center, tr.id_transaction, tr.name||'='||to_char(tr.value)"
			+ "		order by id_transaction"
			+ "	) "
			+ "	group by id_center, id_transaction, first_transaction_date"
			+ "	having count(*) = :columns_quantity"
			+ ") "
			+ "group by id_center, columns "
			+ "having count(*) >= :alert_quantity")
	abstract List<Alert_Result> getResults(
			@Bind("id_center") int id_center,
			@Bind("alert_quantity") int alert_quantity,
			@Bind("columns_quantity") int columns_quantity,
			@Define("baseWhere") String baseWhere
	);

	/**
	 *
	 * @param alert
	 * @return
	 */
	public List<Alert_Result> getResults(Alert alert) {
		String alertRange = "(sysdate - interval '" + alert.getTime_quantity() + "' ";
		switch (alert.getTime_unit()) {
			case AlertTimeUnits.MINUTE:
				alertRange += "MINUTE";
				break;
			case AlertTimeUnits.HOUR:
				alertRange += "HOUR";
				break;
			case AlertTimeUnits.DAY:
				alertRange += "DAY";
				break;
			case AlertTimeUnits.MONTH:
				alertRange = "(ADD_MONTHS(SYSDATE, -" + alert.getTime_quantity() + ")";
				break;
		}
		alertRange += ")";
		String lastActivation = "to_timestamp('" + DATE.toString(alert.getLast_activation()) + "', 'YYYY-MM-DD\"T\"HH24:MI:SS')";
		if (alert.getLast_activation() == null) {
			lastActivation = alertRange;
		}
		String dateCondition = "where t.start_date >= GREATEST(" + lastActivation + "," + alertRange + ")";

		List<Alert_Column> columns = alert.getColumns();
		columns.removeIf(column -> column == null);
		String columnsCondition = "";
		if (!columns.isEmpty()) {
			columnsCondition += "\t\t\t and tr.name in (";
			for (int i = 0; i < columns.size(); i++) {
				Alert_Column column = columns.get(i);
				if (i != 0) {
					columnsCondition += ", ";
				}
				columnsCondition += "'" + column.getColumn() + "'";
			}
			columnsCondition += ")";
		}

		List<Alert_Criteria> criterias = alert.getCriterias();
		criterias.removeIf(criteria -> criteria == null);
		String criteriaConditions = "";
		if (Utils.listNonNullOrEmpty(columns)) {
			for (Alert_Criteria criteria : criterias) {
				if (criteria != null) {
					criteriaConditions += "\t\t\t and ("
							+ " select to_char(value) from transaction_result tr2"
							+ " where tr2.id_transaction=t.id and tr2.name = '" + criteria.getColumn() + "'"
							+ ")  = '" + criteria.getValue() + "'";
				}
			}
		}

		String baseWhere = "\t\t"
				+ dateCondition
				+ columnsCondition
				+ criteriaConditions;
		List<Alert_Result> results = getResults(alert.getId_center(), alert.getQuantity(), columns.size(), baseWhere)
				.stream()
				.map(result -> {
					result.setId_alert(alert.getId());
					return result;
				})
				.collect(Collectors.toList());
		return results;
	}

}
