/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db.dashboard;

/**
 * Clase que representa al conjunto de datos que necesitan los gráficos del
 * Dashboard.
 *
 * @author amatos
 */
public class DashboardItem {

	/**
	 *
	 */
	public Long id;

	/**
	 *
	 */
	public Long id_parent;

	/**
	 *
	 */
	public String label = "";

	/**
	 *
	 */
	public Long value;

	/**
	 *
	 */
	public Double objetive;

	/**
	 *
	 */
	public Double margin;
//	public Double desviation;

	/**
	 *
	 */
	public DashboardItem() {
	}

	/**
	 *
	 * @param id
	 * @param id_parent
	 * @param label
	 * @param value
	 * @param objetive
	 * @param margin
	 */
	public DashboardItem(Long id, Long id_parent, String label, Long value, Double objetive, Double margin
	//, Double desviation
	) {
		this.id = id;
		this.id_parent = id_parent;
		this.label = label;
		this.value = value;
		this.objetive = objetive;
		this.margin = margin;
//		this.desviation = desviation;
	}

}
