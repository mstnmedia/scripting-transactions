/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert_Criteria;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert_criteria}.
 *
 * @author amatos
 */
@RegisterMapper(AlertCriteriaDao.AlertCriteriaMapper.class)
@UseStringTemplate3StatementLocator
public interface AlertCriteriaDao {

	/**
	 *
	 */
	static public class AlertCriteriaMapper implements ResultSetMapper<Alert_Criteria> {

		private static final String ID = "id";
		private static final String ID_ALERT = "id_alert";
		private static final String COLUMN = "column";
		private static final String VALUE = "value";

		@Override
		public Alert_Criteria map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert_Criteria(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_ALERT) ? resultSet.getInt(ID_ALERT) : 0,
					hasColumn.containsKey(COLUMN) ? resultSet.getString(COLUMN) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : ""
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from alert_criteria <where>")
	List<Alert_Criteria> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id_alert
	 * @return
	 */
	@SqlQuery("select * from alert_criteria where id_alert=:id_alert")
	List<Alert_Criteria> getAll(@Bind("id_alert") int id_alert);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from alert_criteria where id = :id")
	Alert_Criteria get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into alert_criteria(id, id_alert, \"column\", value) "
			+ "values(alert_criteria_seq.nextVal, :id_alert, :column, :value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Alert_Criteria item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update alert_criteria set"
			+ " id_alert = :id_alert,"
			+ " \"column\" = :column,"
			+ " value = :value "
			+ "where id = :id")
	void update(@BindBean final Alert_Criteria item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from alert_criteria where id = :id")
	int delete(@Bind("id") final int id);
}
