/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.CategoryTypes;
import com.mstn.scripting.core.models.Category;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code category}.
 *
 * @author amatos
 */
@RegisterMapper(CategoryDao.CategoryMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CategoryDao {

	/**
	 *
	 */
	static public class CategoryMapper implements ResultSetMapper<Category> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_PARENT = "id_parent";
		private static final String ID_TYPE = "id_type";
		private static final String CODE = "code";
		private static final String NAME = "name";
		private static final String CENTER_NAME = "center_name";
		private static final String PARENT_NAME = "parent_name";
		private static final String DELETED = "deleted";

		@Override
		public Category map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Category(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(ID_PARENT) ? resultSet.getInt(ID_PARENT) : 0,
					hasColumn.containsKey(PARENT_NAME) ? resultSet.getString(PARENT_NAME) : "",
					hasColumn.containsKey(ID_TYPE) ? resultSet.getString(ID_TYPE) : "",
					hasColumn.containsKey(CODE) ? resultSet.getString(CODE) : "",
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : false,
					null
			);
		}
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_category <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_category <where> order by ID desc")
	abstract public List<Category> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Category> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, c.* from ("
			+ "  select c.* from v_category c <where> order by <order_by>"
			+ " ) c"
			+ ") c where c.rowno >= ((:page_number - 1) * :page_size + 1) and c.rowno \\<= (:page_number * :page_size)")
	abstract public List<Category> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param id_parent
	 * @return
	 */
	@SqlQuery("select * from v_category where id_parent = :id_parent")
	abstract public List<Category> getAll(@Bind("id_parent") final int id_parent);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_category where id = :id")
	abstract public Category get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into category(id, id_center, id_parent, id_type, code, name, deleted) "
			+ "values(category_seq.nextVal, :id_center, :id_parent, :id_type, :code, :name, :deleted)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Category item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update category set "
			+ " id_center = :id_center, "
			+ " id_parent = :id_parent, "
			+ " id_type = :id_type, "
			+ " code = :code, "
			+ " name = :name, "
			+ " deleted = :deleted "
			+ "where id = :id")
	abstract public void update(@BindBean final Category item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from category where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from category where id_parent=:id),"
			// TODO: Evaluar qué es más eficiente: diferentes subqueries para cada tipo, un solo con 'in (tipos)' o un solo con un or por cada tipo
			+ "	(select count(id) from transaction_result where name='" + CategoryTypes.TYPE1_ID + "' and dbms_lob.compare(value, to_clob(:id))=0),"
			+ "	(select count(id) from transaction_result where name='" + CategoryTypes.TYPE2_ID + "' and dbms_lob.compare(value, to_clob(:id))=0),"
			+ "	(select count(id) from transaction_result where name='" + CategoryTypes.TYPE3_ID + "' and dbms_lob.compare(value, to_clob(:id))=0),"
			+ "	(select count(id) from transaction_result where name like '" + CategoryTypes.PREFIX + CategoryTypes.ACTION + ":id%')"
			+ ")) a")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(final int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
