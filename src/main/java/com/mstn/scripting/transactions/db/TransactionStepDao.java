/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Transaction_Step;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code transaction_step}.
 *
 * @author josesuero
 */
@RegisterMapper(TransactionStepDao.TransactionStepMapper.class)
@UseStringTemplate3StatementLocator
public interface TransactionStepDao {

	/**
	 *
	 */
	public class TransactionStepMapper implements ResultSetMapper<Transaction_Step> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String ID_TRANSACTION = "id_transaction";
		private static final String ID_PARENT = "id_parent";
		private static final String ID_WORKFLOW = "id_workflow";
		private static final String ID_WORKFLOW_STEP = "id_workflow_step";
		private static final String ID_EMPLOYEE = "id_employee";
		private static final String DOCDATE = "docdate";
		private static final String VALUE = "value";
		private static final String ACTIVE = "active";

		@Override
		public Transaction_Step map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Transaction_Step(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_TRANSACTION) ? resultSet.getInt(ID_TRANSACTION) : 0,
					hasColumn.containsKey(ID_PARENT) ? resultSet.getInt(ID_PARENT) : 0,
					hasColumn.containsKey(ID_WORKFLOW) ? resultSet.getInt(ID_WORKFLOW) : 0,
					hasColumn.containsKey(ID_WORKFLOW_STEP) ? resultSet.getInt(ID_WORKFLOW_STEP) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(DOCDATE) ? resultSet.getDate(DOCDATE) : null,
					hasColumn.containsKey(ACTIVE) ? resultSet.getBoolean(ACTIVE) : false,
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : null
			);
		}
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from transaction_step <where> order by id")
	List<Transaction_Step> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from (select * from transaction_step <where> order by id) where rownum=1")
	Transaction_Step getFirst(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from (select * from transaction_step <where> order by id desc) where rownum=1")
	Transaction_Step getLast(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from transaction_step where id = :id order by id")
	Transaction_Step get(@Bind("id") final int id);

	/**
	 *
	 * @param id_transaction
	 * @return
	 */
	@SqlQuery("select * from transaction_step where id_transaction = :id_transaction and active = 1 order by id")
	Transaction_Step getActive(@Bind("id_transaction") final int id_transaction);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into transaction_step(id, id_center, id_transaction, id_parent, id_workflow, id_workflow_step, id_employee, docdate, active, value) values("
			+ "transaction_step_seq.nextVal, :id_center, :id_transaction, :id_parent, :id_workflow, :id_workflow_step, :id_employee, :docdate, :active, :value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Transaction_Step item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update transaction_step set "
			+ " id_center = :id_center, "
			+ " id_transaction = :id_transaction, "
			+ " id_parent = :id_parent, "
			+ " id_workflow = :id_workflow, "
			+ " id_workflow_step = :id_workflow_step, "
			+ " id_employee = :id_employee, "
			+ " docdate = :docdate, "
			+ " active = :active, "
			+ " value = :value "
			+ "where id = :id")
	void update(@BindBean final Transaction_Step item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from transaction_step where id = :id")
	int delete(@Bind("id") final int id);
}
