package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.Transaction;
import com.mstn.scripting.core.models.Workflow_Step;
import java.util.Date;

/**
 * Clase que definiría las propiedades de las notas de las transacciones.
 *
 * @author amatos
 * @deprecated
 */
public class Transaction_Note {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_transaction;

	/**
	 *
	 */
	protected int id_workflow_step;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected int id_client;

	/**
	 *
	 */
	protected int id_service;

	/**
	 *
	 */
	protected Date docdate;

	/**
	 *
	 */
	protected String value;

	private Transaction transaction;
	private Workflow_Step workflow_step;
	private Employee employee;

	/**
	 *
	 */
	public Transaction_Note() {

	}

	/**
	 *
	 * @param id
	 * @param id_transaction
	 * @param id_workflow_step
	 * @param id_employee
	 * @param docdate
	 */
	public Transaction_Note(int id, int id_transaction, int id_workflow_step, int id_employee, Date docdate) {
		this.id = id;
		this.id_transaction = id_transaction;
		this.id_workflow_step = id_workflow_step;
		this.id_employee = id_employee;
		this.docdate = docdate;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_transaction
	 * @param id_workflow_step
	 * @param id_employee
	 * @param docdate
	 * @param transaction
	 * @param workflow_step
	 */
	public Transaction_Note(int id, int id_center, int id_transaction, int id_workflow_step, int id_employee, Date docdate, Transaction transaction, Workflow_Step workflow_step) {
		this.id = id;
		this.id_center = id_center;
		this.id_transaction = id_transaction;
		this.id_workflow_step = id_workflow_step;
		this.id_employee = id_employee;
		this.docdate = docdate;
		this.transaction = transaction;
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_transaction() {
		return id_transaction;
	}

	/**
	 *
	 * @param id_transaction
	 */
	public void setId_transaction(int id_transaction) {
		this.id_transaction = id_transaction;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 *
	 * @param id_workflow_step
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public int getId_client() {
		return id_client;
	}

	/**
	 *
	 * @param id_client
	 */
	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	/**
	 *
	 * @return
	 */
	public int getId_service() {
		return id_service;
	}

	/**
	 *
	 * @param id_service
	 */
	public void setId_service(int id_service) {
		this.id_service = id_service;
	}

	/**
	 *
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 *
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 *
	 * @return
	 */
	public Date getDocdate() {
		return docdate;
	}

	/**
	 *
	 * @param docdate
	 */
	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getWorkflow_step() {
		return workflow_step;
	}

	/**
	 *
	 * @param workflow_step
	 */
	public void setWorkflow_step(Workflow_Step workflow_step) {
		this.workflow_step = workflow_step;
	}

}
