/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.EndCall;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code end_call}.
 *
 * @author amatos
 */
@RegisterMapper(EndCallDao.EndCallMapper.class)
@UseStringTemplate3StatementLocator
public abstract class EndCallDao {

	/**
	 *
	 */
	static public class EndCallMapper implements ResultSetMapper<EndCall> {

		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String DELETED = "deleted";

		@Override
		public EndCall map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new EndCall(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(DELETED) ? resultSet.getBoolean(DELETED) : false
			);
		}
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from end_call <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<EndCall> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, e.* from ("
			+ "  select e.* from end_call e <where> order by <order_by>"
			+ " ) e"
			+ ") e where e.rowno >= ((:page_number - 1) * :page_size + 1) and e.rowno \\<= (:page_number * :page_size)")
	abstract public List<EndCall> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from end_call <where> order by ID desc")
	abstract public List<EndCall> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from end_call where id = :id")
	abstract public EndCall get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @return
	 */
	@SqlUpdate("insert into end_call(id, name, deleted) values(end_call_seq.nextVal, :name, :deleted)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final EndCall item);

	/**
	 *
	 * @param item
	 */
	@SqlUpdate("update end_call set name = :name, deleted = :deleted where id = :id")
	abstract public void update(@BindBean final EndCall item);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from end_call where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from transaction where end_cause=:id)"
			+ ")) a")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(final int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
