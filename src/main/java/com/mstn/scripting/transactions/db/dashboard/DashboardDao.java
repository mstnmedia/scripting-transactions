/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db.dashboard;

import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.db.common.EmployeeDao;
import com.mstn.scripting.core.enums.CategoryTypes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

/**
 * Clase que ejecuta los queries de cálculo de información para los gráficos del
 * Dashboard.
 *
 * @author amatos
 */
@RegisterMapper(DashboardItemMapper.class)
@UseStringTemplate3StatementLocator
public abstract class DashboardDao {

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	static public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = userWheres = new Where("t.id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select to_char(t2.value) label, count(*) value "
			+ "from transaction t "
			+ "	left join transaction_result t1 on t.id=t1.id_transaction "
			+ "	left join transaction_result t2 on t.id=t2.id_transaction "
			+ "<where> "
			+ "group by to_char(t2.value) "
			+ "order by to_char(t2.value)")
	abstract public List<DashboardItem> byProductType(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 */
	public DashboardRepresentation byProductType(User user, List<Where> filters, String type1, String type2) {
		List<Where> baseWhere = Utils.coalesce(filters, new ArrayList());
		baseWhere.add(new Where("t1.name", "=", CategoryTypes.TYPE1_ID));
		baseWhere.add(new Where("t2.name", "=", CategoryTypes.TYPE2_NAME));
		if (Utils.stringNonNullOrEmpty(type1)) {
			baseWhere.add(new Where("t1.value", "=", type1).setType("clob"));
		}
		if (Utils.stringNonNullOrEmpty(type2)) {
			baseWhere.add(new Where("t2.value", "=", type2).setType("clob"));
		}
		WhereClause where = new WhereClause(baseWhere);
		List<DashboardItem> items = byProductType(where.getPreparedString(), where);
		DashboardRepresentation result = new DashboardRepresentation();
		result.setItems(items);
		return result;
	}

	static List<String> ALLOWED_GROUP_BY = Arrays.asList("HH", "DD", "IW", "MM", "YY");

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param groupBy
	 * @return
	 */
	@SqlQuery("select t.start_date label, value "
			+ "from ("
			+ "	select max(t.start_date) start_date, count(*) value "
			+ "	from transaction t "
			+ "		left join transaction_result t1 on t.id=t1.id_transaction "
			+ "		left join transaction_result t2 on t.id=t2.id_transaction "
			+ "	<where> "
			+ "	group by case '<groupBy>' when 'HH' then extract(HOUR from t.start_date) when 'DD' then extract(DAY from t.start_date) when 'IW' then to_number(to_char(t.start_date, 'D')) when 'MM' then extract(MONTH from t.start_date) when 'YY' then extract(YEAR from t.start_date) end"
			+ ") t "
			+ " order by case '<groupBy>' when 'HH' then extract(HOUR from t.start_date) when 'DD' then extract(DAY from t.start_date) when 'IW' then to_number(to_char(t.start_date, 'D')) when 'MM' then extract(MONTH from t.start_date) when 'YY' then extract(YEAR from t.start_date) end")
	abstract public List<DashboardItem> historyByDay(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Define("groupBy") String groupBy
	);

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @param groupBy
	 * @return
	 */
	public DashboardRepresentation historyByDay(User user, List<Where> filters, String type1, String type2, String groupBy) {
		List<Where> baseWhere = Utils.coalesce(filters, new ArrayList());
		baseWhere.add(new Where("t1.name", "=", CategoryTypes.TYPE1_ID));
		baseWhere.add(new Where("t2.name", "=", CategoryTypes.TYPE2_NAME));
		if (Utils.stringNonNullOrEmpty(type1)) {
			baseWhere.add(new Where("t1.value", "=", type1).setType("clob"));
		}
		if (Utils.stringNonNullOrEmpty(type2)) {
			baseWhere.add(new Where("t2.value", "=", type2).setType("clob"));
		}
		WhereClause where = getUserWhere(user, baseWhere);
		DashboardRepresentation result = new DashboardRepresentation();
		if (ALLOWED_GROUP_BY.contains(groupBy)) {
			List<DashboardItem> items = historyByDay(where.getPreparedString(), where, groupBy);
			result.setItems(items);
		}
		return result;
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select to_char(t3.value) label, count(*) value "
			+ "from transaction t "
			+ "	left join transaction_result t1 on t.id=t1.id_transaction "
			+ "	left join transaction_result t2 on t.id=t2.id_transaction "
			+ "	left join transaction_result t3 on t.id=t3.id_transaction "
			+ "<where> "
			+ "group by to_char(t3.value) "
			+ "order by to_char(t3.value)")
	abstract public List<DashboardItem> byCategory(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 */
	public DashboardRepresentation byCategory(User user, List<Where> filters, String type1, String type2) {
		List<Where> baseWhere = Utils.coalesce(filters, new ArrayList());
		baseWhere.add(new Where("t1.name", "=", CategoryTypes.TYPE1_ID));
		baseWhere.add(new Where("t2.name", "=", CategoryTypes.TYPE2_NAME));
		baseWhere.add(new Where("t3.name", "=", CategoryTypes.TYPE3_NAME));
		if (Utils.stringNonNullOrEmpty(type1)) {
			baseWhere.add(new Where("t1.value", "=", type1).setType("clob"));
		}
		if (Utils.stringNonNullOrEmpty(type2)) {
			baseWhere.add(new Where("t2.value", "=", type2).setType("clob"));
		}
		WhereClause where = getUserWhere(user, baseWhere);
		List<DashboardItem> items = byCategory(where.getPreparedString(), where);
		DashboardRepresentation result = new DashboardRepresentation();
		result.setItems(items);
		return result;
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param userID
	 * @return
	 */
	@SqlQuery(EmployeeDao.TREEDOWNBASE
			+ ", base as ("
			+ "	select o.*, "
			+ "		case when objetive!=0 then round(((seconds - objetive) / objetive) * 100, 2) else seconds end desviation, "
			+ "		round((objetive / 100) * 30, 2) margin"
			+ "	from ("
			+ "		select t.id_employee,"
			+ "			round(avg(extract(second from (t.end_date-t.start_date)))) seconds,"
			+ "			("
			+ "				select coalesce(goal_seconds, 0) objetive"
			+ "				from center_manager cm"
			+ "				where cm.id_manager=t.id_employee or cm.id_manager=(select id_parent from treeDown tree where tree.id=t.id_employee)"
			+ "				order by case when cm.id_manager=t.id_employee then 1 else 2 end, cm.id"
			+ "				FETCH NEXT 1 ROWS ONLY"
			+ "			) objetive"
			+ "		from ("
			+ "			select id_employee, state, start_date, end_date"
			+ "			from transaction t"
			+ "			where t.state in (2, 3) and t.id_employee in (select id from treeDown)"
			+ "		) t <where>"
			+ "		group by t.id_employee"
			+ "	) o"
			+ "), results as ("
			+ "	select tree.id, tree.id_parent, tree.name, ("
			+ "		select objetive"
			+ "		from base b "
			+ "		where b.id_employee in (tree.id, tree.id_parent)"
			+ "		ORDER BY case when b.id_employee=tree.id then 1 else 2 end"
			+ "		FETCH NEXT 1 ROWS ONLY"
			+ "	) objetive, ("
			+ "		select round(avg(seconds), 2)"
			+ "		from base b"
			+ "		where b.id_employee in ("
			+ "			select e.id"
			+ "			from treeDown e"
			+ "			connect by nocycle prior id = id_parent"
			+ "			start with id=tree.id"
			+ "		)"
			+ "	) value, ("
			+ "		select round(avg(desviation), 2)"
			+ "		from base b"
			+ "		where b.id_employee in ("
			+ "			select e.id"
			+ "			from treeDown e"
			+ "			connect by nocycle prior id = id_parent"
			+ "			start with id=tree.id"
			+ "		)"
			+ "	) margin"
			+ "	from treeDown tree"
			+ "	where tree.id_parent=:userID or tree.id=:userID "
			+ "		or tree.id in (select distinct del.id_user from delegations del where del.id_delegate=:userID)"
			+ "		or tree.id_parent in (select distinct del.id_user from delegations del where del.id_delegate=:userID)"
			+ ")"
			+ "	select r.id, r.id_parent, r.name label, r.value, ("
			+ "		select coalesce(goal_seconds, 0) objetive"
			+ "		from center_manager cm"
			+ "		where cm.id_manager=r.id or cm.id_manager=r.id_parent "
			+ "		order by case when cm.id_manager=r.id then 1 else 2 end, cm.id"
			+ "		FETCH NEXT 1 ROWS ONLY "
			+ " ) objetive, ("
			+ "		case when (objetive is not null or objetive!=0) and (value is null or value=0) then -100 else margin end "
			+ "	) margin"
			+ "	from results r "
			+ "	ORDER BY case when id=:userID then 1 else 2 end, name"
	)
	abstract public List<DashboardItem> timeAverage(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("userID") long userID
	);

	/**
	 *
	 * @param user
	 * @param filters
	 * @return
	 */
	public DashboardRepresentation timeAverage(User user, List<Where> filters) {
		WhereClause where = new WhereClause(filters);
		long userID = user.getId();
		List<DashboardItem> items = timeAverage(where.getPreparedString(), where, userID);
		DashboardRepresentation result = new DashboardRepresentation();
		result.setItems(items);
		return result;
	}

	/**
	 *
	 * @return
	 */
	@SqlQuery("select 1 from dual")
	abstract public List<DashboardItem> healthCheck();

}
