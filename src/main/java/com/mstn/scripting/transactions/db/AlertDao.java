/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.db;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.ClobValue;
import com.mstn.scripting.core.db.ClobValueBinder;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert}.
 *
 * @author amatos
 */
@RegisterMapper(AlertDao.AlertMapper.class)
@UseStringTemplate3StatementLocator
public abstract class AlertDao {

	/**
	 *
	 */
	static public class AlertMapper implements ResultSetMapper<Alert> {

		private static final String ID = "id";
		private static final String ID_CENTER = "id_center";
		private static final String NAME = "name";
		private static final String TAGS = "tags";
		private static final String NOTE = "note";
		private static final String DOCDATE = "docdate";
		private static final String LAST_ACTIVATION = "last_activation";
		private static final String QUANTITY = "quantity";
		private static final String TIME_UNIT = "time_unit";
		private static final String TIME_QUANTITY = "time_quantity";
		private static final String ACTIVE = "active";
		private static final String EMAILS = "emails";
		private static final String EMAIL_SUBJECT = "email_subject";
		private static final String EMAIL_TEXT = "email_text";

		private static final String CENTER_NAME = "center_name";

		@Override
		public Alert map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(TAGS) ? resultSet.getString(TAGS) : "",
					hasColumn.containsKey(NOTE) ? resultSet.getString(NOTE) : "",
					hasColumn.containsKey(DOCDATE) ? resultSet.getDate(DOCDATE) : null,
					hasColumn.containsKey(LAST_ACTIVATION) ? resultSet.getDate(LAST_ACTIVATION) : null,
					hasColumn.containsKey(QUANTITY) ? resultSet.getInt(QUANTITY) : 0,
					hasColumn.containsKey(TIME_UNIT) ? resultSet.getInt(TIME_UNIT) : 0,
					hasColumn.containsKey(TIME_QUANTITY) ? resultSet.getInt(TIME_QUANTITY) : 0,
					hasColumn.containsKey(ACTIVE) ? resultSet.getInt(ACTIVE) : 0,
					hasColumn.containsKey(EMAILS) ? resultSet.getString(EMAILS) : "",
					hasColumn.containsKey(EMAIL_SUBJECT) ? resultSet.getString(EMAIL_SUBJECT) : "",
					hasColumn.containsKey(EMAIL_TEXT) ? resultSet.getString(EMAIL_TEXT) : "",
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : ""
			);
		}
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select count(id) from v_alert <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @return
	 */
	@SqlQuery("select * from v_alert <where> order by ID desc")
	abstract public List<Alert> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Alert> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, a.* from ("
			+ "  select a.* from v_alert a <where> order by <order_by>"
			+ " ) a"
			+ ") a where a.rowno >= ((:page_number - 1) * :page_size + 1) and a.rowno \\<= (:page_number * :page_size)")
	abstract public List<Alert> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select * from v_alert where id = :id")
	abstract public Alert get(@Bind("id") final int id);

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 * @return
	 */
	@SqlUpdate("insert into alert(id, id_center, name, tags, note, docdate, last_activation, quantity, time_unit, time_quantity, active, emails, email_subject, email_text)"
			+ " values(alert_seq.nextVal, :id_center, :name, :tags, :note, :docdate, :last_activation, :quantity, :time_unit, :time_quantity, :active, :emails, :email_subject, <value>)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Alert item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 * @return
	 */
	public int insert(@BindBean final Alert item) {
		ClobValue value = new ClobValue(item.getEmail_text());
		return insert(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param item
	 * @param value
	 * @param clause
	 */
	@SqlUpdate("update alert set"
			+ " id_center = :id_center,"
			+ " name = :name,"
			+ " tags = :tags,"
			+ " note = :note,"
			+ " docdate = :docdate,"
			+ " last_activation = :last_activation,"
			+ " quantity = :quantity,"
			+ " time_unit = :time_unit,"
			+ " time_quantity = :time_quantity,"
			+ " active = :active,"
			+ " emails = :emails,"
			+ " email_subject = :email_subject, "
			+ " email_text = <value> "
			+ "where id = :id")
	abstract public void update(@BindBean final Alert item,
			@Define("value") String value,
			@ClobValueBinder() ClobValue clause
	);

	/**
	 *
	 * @param item
	 */
	public void update(Alert item) {
		ClobValue value = new ClobValue(item.getEmail_text());
		update(item, value.getPreparedString(), value);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlUpdate("delete from alert where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	@SqlQuery("select sum(column_value) from table(sys.odcinumberlist("
			+ "	(select count(id) from alert_triggered where id_alert=:id)"
			+ ")) a")
	abstract public long dependentsCount(@Bind("id") int id);

	/**
	 *
	 * @param id
	 * @return
	 */
	public boolean hasDependents(final int id) {
		long count = dependentsCount(id);
		return count > 0;
	}
}
