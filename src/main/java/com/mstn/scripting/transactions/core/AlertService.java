/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.db.common.AlertTriggeredService;
import com.mstn.scripting.core.MAIL;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.transactions.db.AlertDao;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.common.AlertTriggeredTransactionDao;
import com.mstn.scripting.core.models.Alert;
import com.mstn.scripting.core.models.Alert_Column;
import com.mstn.scripting.core.models.Alert_Criteria;
import com.mstn.scripting.core.models.Alert_Result;
import com.mstn.scripting.core.models.Alert_Triggered;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import com.mstn.scripting.core.models.Alert_Triggered_Transaction;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.transactions.db.AlertResultDao;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las alertas.
 *
 * @author amatos
 */
public abstract class AlertService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String MISSING_CHILDREN = "item id %s doesn't have children. You must specify before to update data.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public AlertService() {

	}

	private AlertColumnService columnService;

	/**
	 *
	 * @param columnService
	 * @return
	 */
	public AlertService setColumnService(AlertColumnService columnService) {
		this.columnService = columnService;
		return this;
	}

	private AlertCriteriaService criteriaService;

	/**
	 *
	 * @param criteriaService
	 * @return
	 */
	public AlertService setCriteriaService(AlertCriteriaService criteriaService) {
		this.criteriaService = criteriaService;
		return this;
	}
	private AlertTriggeredService triggeredService;

	/**
	 *
	 * @param triggeredService
	 * @return
	 */
	public AlertService setTriggeredService(AlertTriggeredService triggeredService) {
		this.triggeredService = triggeredService;
		return this;
	}

	@CreateSqlObject
	abstract AlertDao dao();

	@CreateSqlObject
	abstract AlertResultDao resultDao();

	@CreateSqlObject
	abstract AlertTriggeredTransactionDao aTTDao();

	/**
	 *
	 * @return
	 */
	@CreateSqlObject
	public abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Alert>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Alert> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Alert> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return loadChildren(
				dao().getAll(where, pageSize, pageNumber, orderBy)
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Alert> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return loadChildren(
				dao().getAll(where.getPreparedString(), where)
		);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Alert getOnly(int id) {
		Alert item = dao().get(id);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Alert get(int id) {
		Alert item = getOnly(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		loadChildren(item);
		return item;
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	public Alert loadChildren(Alert item) {
		item.setColumns(columnService.getAll(item.getId()));
		item.setCriterias(criteriaService.getAll(item.getId()));
		return item;
	}

	/**
	 *
	 * @param list
	 * @return
	 */
	public List<Alert> loadChildren(List<Alert> list) {
		if (Utils.isNullOrEmpty(list)) {
			return list;
		}
		HashMap<Integer, Alert> mapAlerts = new HashMap();
		List<Integer> listIDs = list.stream()
				.map(item -> {
					mapAlerts.put(item.getId(), item);
					item.setColumns(new ArrayList());
					item.setCriterias(new ArrayList());
					return item.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_alert", Where.IN, listIDs));
		List<Alert_Column> columns = columnService.getAll(where);
		columns.forEach(column -> {
			Alert alert = mapAlerts.get(column.getId_alert());
			alert.getColumns().add(column);
		});
		List<Alert_Criteria> criterias = criteriaService.getAll(where);
		criterias.forEach(criteria -> {
			Alert alert = mapAlerts.get(criteria.getId_alert());
			alert.getCriterias().add(criteria);
		});
		return list;
	}

	/**
	 *
	 * @return
	 */
	public List<Alert_Result> getResults() {
		WhereClause where = new WhereClause(new Where("active", 1));
		List<Alert> alerts = getAll(where);
		List<Alert_Result> response = new ArrayList();
		for (int i = 0; i < alerts.size(); i++) {
			Alert alert = alerts.get(i);
			List<Alert_Result> results = resultDao().getResults(alert);
			response.addAll(results);
		}
		return response;
	}

	/**
	 *
	 * @param results
	 * @param user
	 * @return
	 */
	public List<Alert_Triggered> triggerAlerts(List<Alert_Result> results, User user) {
		List<Alert_Triggered> currents = triggeredService.getAll(
				new WhereClause(new Where("state", "1"))
		);
		List<Alert_Triggered> list = new ArrayList();
		for (int i = 0; i < results.size(); i++) {
			Alert_Result result = results.get(i);
			//Obteniendo datos de la alerta a disparar
			Alert_Triggered item = new Alert_Triggered(
					0, result.getId_center(), "", result.getId_alert(), "",
					new Date(), null, 1, result.getFirst_transaction_date(),
					result.getQuantity(), "", "", "", user.getId()
			);
			List<String> columns = Utils.splitAsList(result.getColumns());
			for (int j = 0; j < columns.size(); j++) {
				String column = columns.get(j);
				List<String> name_value = Utils.splitAsList(column, "=");
				String name = name_value.size() > 0 ? name_value.get(0) : "";
				String value = name_value.size() > 1 ? name_value.get(1) : "";

				Alert_Triggered_Data data = new Alert_Triggered_Data(0, 0, 0, name, value);
				item.getData().add(data);
			}

			//Ignorando si ya existe una alerta activa del mismo tipo
			List<Alert_Triggered_Data> iDatas = item.getData();
			Alert_Triggered duplicated = currents.stream().filter(a -> {
				if (a.getId_center() != item.getId_center() || a.getId_alert() != item.getId_alert()) {
					return false;
				}
				for (int k = 0; k < iDatas.size(); k++) {
					Alert_Triggered_Data iData = iDatas.get(k);
					Alert_Triggered_Data sData = a.getData().stream()
							.filter(d -> {
								return Utils.stringAreEquals(iData.getName(), d.getName())
										&& Utils.stringAreEquals(iData.getValue(), d.getValue());
							})
							.findFirst()
							.orElse(null);
					if (sData == null) {
						return false;
					}
				}
				return true;
			}).findFirst().orElse(null);

			if (duplicated != null) {
				continue;
			}

			//Registrando alerta disparada con columnas
			Alert_Triggered triggered = triggeredService.create(item, user);
			list.add(triggered);
		}
		loadAlerts(list);
		// Enviando notificación de estas alertas disparadas
		sendMailAlerts(list, user);
		return list;
	}

	/**
	 *
	 * @param triggereds
	 * @param user
	 */
	public void sendMailAlerts(List<Alert_Triggered> triggereds, User user) {
		for (Alert_Triggered triggered : triggereds) {
			try {
				Alert alert = triggered.getAlert();
				String to = alert.getEmails();
				List<Alert_Triggered_Data> datas = Utils.coalesce(triggered.getData(), new ArrayList());
				List<Alert_Triggered_Transaction> transactions = Utils.coalesce(triggered.getTransaction(), new ArrayList());
				addAlertData(alert, datas);
				addTriggeredData(triggered, datas);
				Map<String, Object> values = new HashMap();
				for (Alert_Triggered_Data data : datas) {
					values.put(data.getName(), Utils.coalesce(data.getValue(), Transaction_Result.NULL_VALUE));
				}
				addTransactionsData(transactions, values);
				values.put("alert", alert);
				values.put("alertTriggered", triggered);
				values.put("datas", datas);
				values.put("transactions", transactions);
				String subject = alert.getEmail_subject();
				try {
					subject = Velocity.interpolate(subject, values);
				} catch (Exception ex) {
					Utils.logException(this.getClass(),
							"Error interpolating mail subject of alert " + alert.getId() + " " + alert.getName(),
							ex);
					log().insert(user.getId(), "Alertas disparadas", triggered.getId(), "Error",
							"Error interpolando el texto del asunto del correo: " + ex.getMessage(), ex, user.getIp());
				}
				String content = alert.getEmail_text();
				try {
					content = Velocity.interpolate(content, values);
				} catch (Exception ex) {
					Utils.logException(this.getClass(),
							"Error interpolating mail content of alert " + alert.getId() + " " + alert.getName(),
							ex);
					log().insert(user.getId(), "Alertas disparadas", triggered.getId(), "Error",
							"Error interpolando el texto del cuerpo del correo: " + ex.getMessage(), ex, user.getIp());
				}
				MAIL.send(to, subject, content);
			} catch (Exception ex) {
				Utils.logException(this.getClass(), "Error while send mail for alerts", ex);
				log().insert(user.getId(), "Alertas disparadas", triggered.getId(), "Error",
						"Error intentando enviar correo: " + ex.getMessage(), ex, user.getIp());
			}
		}
	}

	/**
	 *
	 * @param triggereds
	 * @return
	 */
	public List<Alert_Triggered> loadAlerts(List<Alert_Triggered> triggereds) {
		// TODO: Upgrade performance here
		for (int i = 0; i < triggereds.size(); i++) {
			Alert_Triggered triggered = triggereds.get(i);
			Alert alert = getOnly(triggered.getId_alert());
			triggered.setAlert(alert);
		}
		return triggereds;
	}

	/**
	 *
	 * @param item
	 * @param datas
	 */
	public void addAlertData(Alert item, List<Alert_Triggered_Data> datas) {
		datas.add(new Alert_Triggered_Data("alert_active", String.valueOf(item.getActive())));
		datas.add(new Alert_Triggered_Data("alert_center_name", String.valueOf(item.getCenter_name())));
		datas.add(new Alert_Triggered_Data("alert_docdate", dateToString(item.getDocdate())));
		datas.add(new Alert_Triggered_Data("alert_id", String.valueOf(item.getId())));
		datas.add(new Alert_Triggered_Data("alert_id_center", String.valueOf(item.getId_center())));
		datas.add(new Alert_Triggered_Data("alert_last_activation", dateToString(item.getLast_activation())));
		datas.add(new Alert_Triggered_Data("alert_name", String.valueOf(item.getName())));
		datas.add(new Alert_Triggered_Data("alert_note", String.valueOf(item.getNote())));
		datas.add(new Alert_Triggered_Data("alert_quantity", String.valueOf(item.getQuantity())));
		datas.add(new Alert_Triggered_Data("alert_tags", String.valueOf(item.getTags())));
		datas.add(new Alert_Triggered_Data("alert_time_quantity", String.valueOf(item.getTime_quantity())));
		datas.add(new Alert_Triggered_Data("alert_time_unit", String.valueOf(item.getTime_unit())));
	}

	/**
	 *
	 * @param item
	 * @param datas
	 */
	public void addTriggeredData(Alert_Triggered item, List<Alert_Triggered_Data> datas) {
		datas.add(new Alert_Triggered_Data("triggered_alert_name", String.valueOf(item.getAlert_name())));
		datas.add(new Alert_Triggered_Data("triggered_action", String.valueOf(item.getAction())));
		datas.add(new Alert_Triggered_Data("triggered_center_name", String.valueOf(item.getCenter_name())));
		datas.add(new Alert_Triggered_Data("triggered_description", String.valueOf(item.getDescription())));
		datas.add(new Alert_Triggered_Data("triggered_end_date", dateToString(item.getEnd_date())));
		datas.add(new Alert_Triggered_Data("triggered_first_transaction_date", dateToString(item.getFirst_transaction_date())));
		datas.add(new Alert_Triggered_Data("triggered_id", String.valueOf(item.getId())));
		datas.add(new Alert_Triggered_Data("triggered_id_alert", String.valueOf(item.getId_alert())));
		datas.add(new Alert_Triggered_Data("triggered_id_center", String.valueOf(item.getId_center())));
		datas.add(new Alert_Triggered_Data("triggered_id_employee", String.valueOf(item.getId_employee())));
		datas.add(new Alert_Triggered_Data("triggered_parent_ticket", String.valueOf(item.getParent_ticket())));
		datas.add(new Alert_Triggered_Data("triggered_start_date", dateToString(item.getStart_date())));
		datas.add(new Alert_Triggered_Data("triggered_state", String.valueOf(item.getState())));
		datas.add(new Alert_Triggered_Data("triggered_transactions_count", String.valueOf(item.getTransactions_count())));
	}

	/**
	 *
	 * @param transactions
	 * @param values
	 */
	public void addTransactionsData(List<Alert_Triggered_Transaction> transactions, Map values) {
		values.put("transactions_size", transactions.size());
		values.put("transactions_items", JSON.toString(transactions));
		for (int i = 0; i < transactions.size(); i++) {
			Alert_Triggered_Transaction transaction = transactions.get(i);
			values.put("transactions" + i + "_id", transaction.getId());
			values.put("transactions" + i + "_id_alert_triggered", transaction.getId_alert_triggered());
			values.put("transactions" + i + "_id_transaction", transaction.getId_transaction());
			values.put("transactions" + i + "_id_alert", transaction.getId_alert());
			values.put("transactions" + i + "_alert_name", Utils.coalesce(transaction.getAlert_name(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_id_center", transaction.getId_center());
			values.put("transactions" + i + "_center_name", Utils.coalesce(transaction.getCenter_name(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_id_employee", transaction.getId_employee());
			values.put("transactions" + i + "_employee_name", Utils.coalesce(transaction.getEmployee_name(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_workflow_name", Utils.coalesce(transaction.getWorkflow_name(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_version_id_version", Utils.coalesce(transaction.getVersion_id_version(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_subscriber_no", Utils.coalesce(transaction.getSubscriber_no(), Transaction_Result.NULL_VALUE));
			values.put("transactions" + i + "_start_date", dateToString(transaction.getStart_date()));
			values.put("transactions" + i + "_end_date", dateToString(transaction.getEnd_date()));
			values.put("transactions" + i + "_end_cause_name", Utils.coalesce(transaction.getEnd_cause_name(), Transaction_Result.NULL_VALUE));
		}
	}

	/**
	 *
	 * @param date
	 * @return
	 */
	public String dateToString(Date date) {
		return date != null
				? DATE.toString(date)//DATE.format(date, "dd/MM/yyyy hh:mm a")
				: Transaction_Result.NULL_VALUE;
	}

	/**
	 *
	 * @param item
	 */
	public void validateItem(Alert item) {
		if (item.getActive() == 1 && Utils.listIsNullOrEmpty(item.getColumns())) {
			throw new WebApplicationException(
					String.format(MISSING_CHILDREN, item.getId()),
					Response.Status.BAD_REQUEST);
		}
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Alert create(Alert item, User user) {
		validateItem(item);
		item.setDocdate(new Timestamp(new Date().getTime()));
		if (item.getActive() == 1) {
			item.setLast_activation(new Timestamp(new Date().getTime()));
		}
		if (!user.isMaster() || item.getId_center() == 0) {
			item.setId_center(user.getCenterId());
		}
		int id = dao().insert(item);
		item.setId(id);
		log().insert(user.getId(), "Alertas", id, "Crear", item, item, user.getIp());

		List<Alert_Column> columns = item.getColumns();
		columns.forEach(column -> {
			column.setId_alert(item.getId());
			columnService.create(column, user);
		});
		List<Alert_Criteria> criterias = item.getCriterias();
		criterias.forEach(criteria -> {
			criteria.setId_alert(item.getId());
			criteriaService.create(criteria, user);
		});
		return get(id);
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Alert update(Alert item, User user) {
		validateItem(item);
		Alert before = get(item.getId());
		if (!user.isMaster() || item.getId_center() == 0) {
			item.setId_center(before.getId_center());
		}
		item.setDocdate(before.getDocdate());
		if (item.getActive() == 1) {
			item.setLast_activation(new Timestamp(new Date().getTime()));
		} else {
			item.setLast_activation(before.getLast_activation());
		}
		dao().update(item);
		updateColumns(item, user);
		updateCriterias(item, user);

		Alert after = get(item.getId());
		log().insert(user.getId(), "Alertas", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 */
	public void updateColumns(Alert item, User user) {
		dao().update(item);
		List<Alert_Column> oldColumns = columnService.getAll(item.getId());
		List<Alert_Column> newColumns = item.getColumns();

		for (int i = 0; i < oldColumns.size(); i++) {
			Alert_Column oldC = oldColumns.get(i);
			boolean found = false;
			for (int j = 0; j < newColumns.size(); j++) {
				Alert_Column newC = newColumns.get(j);
				boolean match = Utils.stringAreEquals(oldC.getColumn(), oldC.getColumn());
				if (match) {
					newC.setId(oldC.getId());
					newC.setId_alert(oldC.getId_alert());
					columnService.update(newC, user);
					newColumns.remove(j);
					found = true;
					break;
				}
			}
			if (!found) {
				columnService.delete(oldC.getId(), user);
			}
		}
		for (int j = 0; j < newColumns.size(); j++) {
			Alert_Column newC = newColumns.get(j);
			newC.setId_alert(item.getId());
			columnService.create(newC, user);
		}
	}

	/**
	 *
	 * @param item
	 * @param user
	 */
	public void updateCriterias(Alert item, User user) {
		dao().update(item);
		List<Alert_Criteria> oldCriterias = criteriaService.getAll(item.getId());
		List<Alert_Criteria> newCriterias = item.getCriterias();

		for (int i = 0; i < oldCriterias.size(); i++) {
			Alert_Criteria oldC = oldCriterias.get(i);
			boolean found = false;
			for (int j = 0; j < newCriterias.size(); j++) {
				Alert_Criteria newC = newCriterias.get(j);
				boolean match = Utils.stringAreEquals(oldC.getColumn(), oldC.getColumn());
				if (match) {
					newC.setId(oldC.getId());
					newC.setId_alert(oldC.getId_alert());
					criteriaService.update(newC, user);
					newCriterias.remove(j);
					found = true;
					break;
				}
			}
			if (!found) {
				criteriaService.delete(oldC.getId(), user);
			}
		}
		for (int j = 0; j < newCriterias.size(); j++) {
			Alert_Criteria newC = newCriterias.get(j);
			newC.setId_alert(item.getId());
			criteriaService.create(newC, user);
		}
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(int id, User user) {
		Alert before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Alertas", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Response.Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
