/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.transactions.db.dashboard.DashboardDao;
import com.mstn.scripting.transactions.db.dashboard.DashboardRepresentation;
import java.util.List;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase que realiza el cálculo para los gráficos del Dashboard.
 *
 * @author amatos
 */
public abstract class DashboardService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public DashboardService() {

	}

	@CreateSqlObject
	abstract DashboardDao dao();

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 */
	public DashboardRepresentation byProductType(User user, List<Where> filters, String type1, String type2) {
		DashboardRepresentation result = dao().byProductType(user, filters, type1, type2);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @param groupBy
	 * @return
	 */
	public DashboardRepresentation historyByDay(
			User user, List<Where> filters,
			String type1, String type2, String groupBy) {
		DashboardRepresentation result = dao().historyByDay(user, filters, type1, type2, groupBy);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @param type1
	 * @param type2
	 * @return
	 */
	public DashboardRepresentation byCategory(User user, List<Where> filters, String type1, String type2) {
		DashboardRepresentation result = dao().byCategory(user, filters, type1, type2);
		return result;
	}

	/**
	 *
	 * @param user
	 * @param filters
	 * @return
	 */
	public DashboardRepresentation timeAverage(User user, List<Where> filters) {
		DashboardRepresentation result = dao().timeAverage(user, filters);
		return result;
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().healthCheck();
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
