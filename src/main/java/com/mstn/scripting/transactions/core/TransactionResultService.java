/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.transactions.db.TransactionResultDao;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las variables de transacciones.
 *
 * @author amatos
 */
public abstract class TransactionResultService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public TransactionResultService() {
	}

	@CreateSqlObject
	abstract TransactionResultDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = userWheres = new Where(
				"(select t.id_employee from transaction t where t.id=tr.id_transaction)",
				user.getId()
		);
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Transaction_Result> getAll(WhereClause where) {
		where = where == null ? new WhereClause() : where;
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_transaction
	 * @return
	 */
	public List<Transaction_Result> getAllByTransaction(int id_transaction) {
		return dao().getAllByTransaction(id_transaction);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Transaction_Result get(int id) {
		Transaction_Result item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 *
	 * @param id_transaction
	 * @param name
	 * @return
	 */
	public Transaction_Result getByName(int id_transaction, String name) {
		return dao().getByName(id_transaction, name);
	}

	/**
	 *
	 * @param item
	 * @param user
	 */
	public void createOrUpdate(Transaction_Result item, User user) {
		// TODO: ¿Sería más eficiente usar un IF en la base de datos?
		Transaction_Result before = getByName(item.getId_transaction(), item.getName());
		if (before != null) {
			InterfaceConfigs.log(this.getClass(), "result: " + item.getName() + "; current: " + before.getId() + "; value: " + item.getValue());
			item.setId(before.getId());
			updateOnly(item, before, user);
		} else {
			InterfaceConfigs.log(this.getClass(), "result: " + item.getName() + "; current: 0; value: " + item.getValue());
			createOnly(item, user);
		}
	}

//	public Transaction_Result create(Transaction_Result item) {
//		int id = createOnly(item);
//		return get(id);
//	}
	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public int createOnly(Transaction_Result item, User user) {
		int id = dao().insert(item);
		item.setId(id);
		log().insert(user.getId(), "Variables de transacciones", item.getId(), "Crear", item, null, user.getIp());
		return id;
	}

	/**
	 *
	 * @param item
	 * @param before
	 * @param user
	 */
	public void updateOnly(Transaction_Result item, Transaction_Result before, User user) {
		dao().update(item);
		log().insert(user.getId(), "Variables de transacciones", item.getId(), "Actualizar", before, item, user.getIp());
	}

//	public Transaction_Result update(Transaction_Result item) {
//		get(item.getId());
//		updateOnly(item);
//		return get(item.getId());
//	}
//
	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public int delete(int id, User user) {
		Transaction_Result before = get(id);
		int result = dao().delete(id);
		log().insert(user.getId(), "Variables de transacciones", before.getId(), "Eliminar", before, null, user.getIp());
		return result;
	}

	/**
	 *
	 * @param id_transaction
	 * @param id_interface
	 * @return
	 */
	public int delete(int id_transaction, int id_interface) {
		return dao().delete(id_transaction, id_interface);
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
}
