/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.db.common.AlertTriggeredService;
import com.mstn.scripting.core.db.common.AlertTriggeredTransactionService;
import com.mstn.scripting.core.enums.CategoryTypes;
import com.mstn.scripting.core.enums.DateFormat;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionEndCauses;
import com.mstn.scripting.core.enums.TransactionStates;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import com.mstn.scripting.core.enums.WorkflowStepTypes;
import com.mstn.scripting.core.models.Alert_Triggered;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import com.mstn.scripting.core.models.Alert_Triggered_Transaction;
import com.mstn.scripting.core.models.Category;
import com.mstn.scripting.core.models.Content;
import com.mstn.scripting.core.models.IdValueParams;
import com.mstn.scripting.core.models.Interface;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.core.models.TransactionInterfacesPayload;
import com.mstn.scripting.core.models.TransactionInterfacesResponse;
import com.mstn.scripting.core.models.Transaction_Result;
import com.mstn.scripting.core.models.Transaction_Step;
import com.mstn.scripting.core.models.V_Transaction;
import com.mstn.scripting.core.models.Workflow;
import com.mstn.scripting.core.models.Workflow_Step;
import com.mstn.scripting.core.models.Workflow_Step_Content;
import com.mstn.scripting.core.models.Workflow_Step_Option;
import com.mstn.scripting.transactions.ScriptingTransactionsApplication;
import com.mstn.scripting.transactions.ScriptingTransactionsConfiguration;
import com.mstn.scripting.transactions.db.TransactionStepDao;
import com.mstn.scripting.transactions.db.V_TransactionDao;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.WebApplicationException;
import org.apache.http.client.HttpClient;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con los procesos de las transacciones: crea, avanza,
 * retrocede, cancela y actualiza una transacción.
 *
 * @author amatos
 */
public abstract class V_TransactionService {

	private static final String NOT_FOUND = "El registro #%s no se ha encontrado.";
	private static final String CANT_CREATE_TRANSACTION_WITH_CENTER = "No puede iniciar una transacción si no tiene centro asignado.";
	private static final String TRANSACTION_NOT_MODIFIABLE = "No se puede continuar con esta transacción. Fue finalizada por %s el %s.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";

	private final int ID_GREETING_CONTENT = -1;

	private ScriptingTransactionsConfiguration config;
	private HttpClient httpClient;
	private TransactionStepService transactionStepService;
	private V_TransactionStepService vTransactionStepService;
	private TransactionResultService transactionResultService;
	private AlertTriggeredService alertTriggeredService;
//	private AlertTriggeredDataService alertTriggeredDataService;
	private AlertTriggeredTransactionService alertTriggeredTransactionService;
	private CategoryService categoryService;

	/**
	 *
	 */
	public V_TransactionService() {
	}

	/**
	 *
	 * @return
	 */
	public ScriptingTransactionsConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public V_TransactionService setConfig(ScriptingTransactionsConfiguration config) {
		this.config = config;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public V_TransactionService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	/**
	 *
	 * @param transactionStepService
	 * @return
	 */
	public V_TransactionService setTransactionStepService(TransactionStepService transactionStepService) {
		this.transactionStepService = transactionStepService;
		return this;
	}

	/**
	 *
	 * @param vTransactionStepService
	 * @return
	 */
	public V_TransactionService setV_TransactionStepService(V_TransactionStepService vTransactionStepService) {
		this.vTransactionStepService = vTransactionStepService;
		return this;
	}

	/**
	 *
	 * @param transactionResultService
	 * @return
	 */
	public V_TransactionService setTransactionResultService(TransactionResultService transactionResultService) {
		this.transactionResultService = transactionResultService;
		return this;
	}

	/**
	 *
	 * @param categoryService
	 * @return
	 */
	public V_TransactionService setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
		return this;
	}

	/**
	 *
	 * @param alertTriggeredService
	 * @param alertTriggeredTransactionService
	 * @return
	 */
	public V_TransactionService setAlertsService(
			AlertTriggeredService alertTriggeredService,
			//			AlertTriggeredDataService alertTriggeredDataService,
			AlertTriggeredTransactionService alertTriggeredTransactionService) {
		this.alertTriggeredService = alertTriggeredService;
//		this.alertTriggeredDataService = alertTriggeredDataService;
		this.alertTriggeredTransactionService = alertTriggeredTransactionService;
		return this;
	}

	@CreateSqlObject
	abstract V_TransactionDao dao();

	@CreateSqlObject
	abstract LogDao log();

	@CreateSqlObject
	abstract TransactionStepDao transactionStepDao();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = null;
		if (!user.isMaster()) {
			List<Integer> treeDown = ScriptingTransactionsApplication.EmployeeService
					.getEmployeeTreeDownInt(user.getId());
			userWheres = new Where("id_employee", Where.IN, treeDown);
		}
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param transaction
	 */
	public void validateRunningTransaction(V_Transaction transaction) {
		if (transaction.getState() != TransactionStates.RUNNING.getValue()) {
			String endDate = DATE.format(transaction.getEnd_date(), DateFormat.DDMMYYYY_A_LAS_HHMMAA_FORMAT);
			throw new WebApplicationException(
					String.format(TRANSACTION_NOT_MODIFIABLE, transaction.getEnd_employee_name(), endDate),
					HttpStatus.NOT_ACCEPTABLE_406);
		}
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<V_Transaction>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<V_Transaction> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<V_Transaction> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_customer
	 * @return
	 */
	public List<V_Transaction> getByCustomer(String id_customer) {
		return dao().getByCustomer(id_customer);
	}

	/**
	 *
	 * @param id_employee
	 * @param id_version
	 * @return
	 */
	public long usedVersion(int id_employee, int id_version) {
		WhereClause where = new WhereClause(
				new Where("id_employee", String.valueOf(id_employee)),
				new Where("id_workflow_version", String.valueOf(id_version))
		);
		long count = getCount(where);
		return count;
	}

	/**
	 *
	 * @param user
	 * @param id_employee
	 * @return
	 */
	public List<V_Transaction> activeTransactions(User user, int id_employee) {
		WhereClause where = getUserWhere(user, Arrays.asList(
				new Where("state", String.valueOf(TransactionStates.RUNNING.getValue()))
		));
		List<V_Transaction> list = getAll(where);
		return list;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public V_Transaction get(int id, User user) {
		long start = DATE.nowAsLong();
		V_Transaction item = dao().get(id);
		long getItem = DATE.nowAsLong() - start;
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), HttpStatus.NOT_FOUND_404);
		}
		long found = DATE.nowAsLong() - getItem;
		if (user != null && item.getId_employee() != user.getId() && !user.isMaster()) {
			List<Integer> treeDown = ScriptingTransactionsApplication.EmployeeService
					.getEmployeeTreeDownInt(user.getId());
			long getTreeDown = DATE.nowAsLong() - found;
			if (!treeDown.contains(item.getId_employee())) {
				throw new WebApplicationException("Esta transacción no pertenece a tu cadena de mando.", HttpStatus.FORBIDDEN_403);
			}
			long access = DATE.nowAsLong() - getTreeDown;
		}
		long returning = DATE.nowAsLong() - found;
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadResults
	 * @param loadSteps
	 * @param user
	 * @return
	 */
	public V_Transaction get(int id, boolean loadResults, boolean loadSteps, User user) {
		V_Transaction item = get(id, user);
		if (loadResults) {
			item.setTransaction_results(
					transactionResultService.getAllByTransaction(item.getId())
			);
		}
		if (loadSteps) {
			item.setTransaction_steps(
					vTransactionStepService.getAllByTransaction(item.getId())
			);
		}
		return item;
	}

	// TODO: Debería hacer este proceso dentro de una transacción en la base de datos;
	//		pero el proceso se queda pausado insertando la primera variable.
	//@org.skife.jdbi.v2.sqlobject.Transaction
	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public V_Transaction create(V_Transaction item, User user) throws Exception {
		int id = 0;
		try {
			if (user.getCenter() == null) {
				throw new WebApplicationException(CANT_CREATE_TRANSACTION_WITH_CENTER, HttpStatus.NOT_ACCEPTABLE_406);
			}
			item.setId_center(user.getCenterId());
			item.setId_employee(user.getId());
			item.setEmployee_name(user.getName());
			if (item.getStart_date() == null || item.getStart_date().getTime() > DATE.nowAsLong()) {
				item.setStart_date(Date.from(Instant.now()));
			}
			if (item.getState() == 0) {
				item.setState(TransactionStates.RUNNING.getValue());
			} else if (item.getState() == TransactionStates.COMPLETED.getValue()
					|| item.getState() == TransactionStates.CANCELLED.getValue()) {
				if (item.getEnd_date() == null || item.getEnd_date().getTime() > DATE.nowAsLong()) {
					item.setEnd_date(Date.from(Instant.now()));
				}
				if (item.getEnd_id_employee() == 0) {
					item.setEnd_id_employee(item.getId_employee());
					item.setEnd_employee_name(item.getEmployee_name());
				}
			}
			if (item.getId_call() == null || item.getId_call().isEmpty()) {
				String GUID = java.util.UUID.randomUUID().toString();
				item.setId_call(GUID);
			}

			Workflow workflow = Workflow.getActive(item.getId_workflow(), httpClient, config.getApiURL(), user.getToken());
			item.setId_workflow_version(workflow.getVersion_id());

			Workflow_Step firstStep = Workflow_Step.getFirstStep(workflow.getVersion_id(), httpClient, config.getApiURL(), user.getToken());
			item.setId_current_step(firstStep.getId());
			boolean nextStepIsWF = firstStep.getId_type() == WorkflowStepTypes.WORKFLOW.getValue();
			id = dao().insert(item);
			item.setId(id);

			V_Transaction after = get(id, null);
			after.setEmployee_name(item.getEmployee_name());
			after.setEnd_employee_name(item.getEnd_employee_name());
			copyChildren(after, item);

			Transaction_Step transactionStep = new Transaction_Step(
					0, after.getId_center(), after.getId(), 0, workflow.getId(), firstStep.getId(),
					after.getId_employee(), after.getStart_date(),
					!nextStepIsWF, //after.getState() == TransactionStates.RUNNING.getValue(),
					null
			);
			int transactionStepID = transactionStepDao().insert(transactionStep);
			transactionStep.setId(transactionStepID);
			if (nextStepIsWF) {
				setSubNextStep(after, firstStep, transactionStep, user);
				update(after, user);
			}
			List<Transaction_Result> results = after.getTransaction_results();
			saveResults(after, results, true, user);
			updateTransactionVariables(after, user);

			log().insert(user.getId(), "Transacciones", id, "Crear", item, after, user.getIp());
			return after;
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", id, "Error", "Error creando transacción: " + ex.getMessage(), ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @param params
	 * @param user
	 * @return
	 */
	public boolean updateNote(IdValueParams params, User user) {
		try {
			V_Transaction transaction = get(params.getId(), user);
			validateRunningTransaction(transaction);
			dao().updateNote(params.getId(), params.getValue());
			dao().updateNoteVariable(params.getId(), params.getValue());
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", params.getId(), "Error",
					"Error actualizando nota de la transacción: " + ex.getMessage(), Arrays.asList(params, ex),
					user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
		return true;
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 */
	public void update(V_Transaction transaction, User user) {
		dao().update(transaction);
		log().insert(user.getId(), "Transacciones", transaction.getId(), "Actualizar", transaction, null, user.getIp());
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 */
	public void updateWithVariables(V_Transaction transaction, User user) {
		dao().update(transaction);
		log().insert(user.getId(), "Transacciones", transaction.getId(), "Actualizar", transaction, null, user.getIp());
		updateTransactionVariables(transaction.getId(), user);
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 * @return
	 */
	public V_Transaction updateWithStepVariables(V_Transaction transaction, User user) {
		dao().update(transaction);
		V_Transaction after = get(transaction.getId(), null);
		log().insert(user.getId(), "Transacciones", transaction.getId(), "Actualizar", transaction, after, user.getIp());
		List<Transaction_Result> transactionResults = V_Transaction.getStepResults(after);
		saveResults(transaction, transactionResults, user);
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 * @return
	 */
	public V_Transaction updateWithEndVariables(V_Transaction transaction, User user) {
		dao().update(transaction);
		V_Transaction after = get(transaction.getId(), null);
		after.setEnd_employee_name(transaction.getEnd_employee_name());
		log().insert(user.getId(), "Transacciones", transaction.getId(), "Actualizar", transaction, after, user.getIp());
		List<Transaction_Result> endResults = V_Transaction.getEndResults(after);
		saveResults(transaction, endResults, user);
		matchWithTriggereds(transaction, user);
		return transaction;
	}

	/**
	 *
	 * @param id_transaction
	 * @param user
	 * @return
	 */
	public V_Transaction updateTransactionVariables(int id_transaction, User user) {
		V_Transaction transaction = get(id_transaction, null);
		return updateTransactionVariables(transaction, user);
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 * @return
	 */
	public V_Transaction updateTransactionVariables(V_Transaction transaction, User user) {
		List<Transaction_Result> transactionResults = V_Transaction.getResults(transaction);
		saveResults(transaction, transactionResults, user);
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 * @param user
	 */
	public void matchWithTriggereds(V_Transaction transaction, User user) {
		List<Alert_Triggered> triggereds = alertTriggeredService.getAll(
				new WhereClause(new Where("state", 1))
		);
		if (!triggereds.isEmpty()) {
			List<Transaction_Result> transactionResults = transactionResultService.getAllByTransaction(transaction.getId());
			Map<String, String> mapResults = new HashMap();
			for (Transaction_Result result : transactionResults) {
				mapResults.put(result.getName(), Utils.coalesce(result.getValue(), Transaction_Result.NULL_VALUE));
			}
			// TODO: Probar el comportamiento cuando dispara una alerta de valores nulos o vacíos
			List<Alert_Triggered> matcheds = triggereds.stream()
					.filter(triggered -> {
						if (triggered.getData() == null || triggered.getData().isEmpty()) {
							return false;
						}
						for (Alert_Triggered_Data data : triggered.getData()) {
							if (!Utils.stringAreEquals(data.getValue(), mapResults.get(data.getName()))) {
								return false;
							}
						}
						return true;
					})
					.collect(Collectors.toList());
			if (!matcheds.isEmpty()) {
				HashMap<Integer, Alert_Triggered> mapMatcheds = new HashMap();
				List<Integer> matchedsIDs = matcheds.stream()
						.map(item -> {
							item.setTransaction(new ArrayList());
							mapMatcheds.put(item.getId(), item);
							return item.getId();
						})
						.collect(Collectors.toList());
				WhereClause where = new WhereClause(new Where("id_alert_triggered", Where.IN, matchedsIDs));
				List<Alert_Triggered_Transaction> matchedTransactions = alertTriggeredTransactionService.getAll(where);
				matchedTransactions.forEach(item -> {
					Alert_Triggered matched = mapMatcheds.get(item.getId_alert_triggered());
					matched.getTransaction().add(item);
				});

				for (int i = 0; i < matcheds.size(); i++) {
					Alert_Triggered matched = matcheds.get(i);
					List<Alert_Triggered_Transaction> transactions = matched.getTransaction();
					boolean relationed = false;
					for (int j = 0; j < transactions.size(); j++) {
						Alert_Triggered_Transaction t = transactions.get(j);
						if (t.getId_transaction() == transaction.getId()) {
							relationed = true;
							break;
						}
					}
					if (!relationed) {
						Alert_Triggered_Transaction item = new Alert_Triggered_Transaction(0, matched.getId(), transaction.getId(), false);
						alertTriggeredTransactionService.create(item, user);
					}
				}
			}
		}
	}

	/**
	 *
	 * @param id_transaction
	 * @param cause
	 * @param note
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transaction
	public boolean endCall(int id_transaction, int cause, String note, User user) throws Exception {
		long start = DATE.nowAsLong();
		try {
			V_Transaction transaction = get(id_transaction, user);
			long GT = DATE.nowAsLong();
			Utils.logInfo(this.getClass(), "GT with " + (GT - start));
			validateRunningTransaction(transaction);
			transaction.setState(TransactionStates.CANCELLED.getValue());
			transaction.setEnd_cause(cause);
			transaction.setEnd_date(Date.from(Instant.now()));
			transaction.setEnd_id_employee(user.getId());
			transaction.setEnd_employee_name(user.getName());
			transaction.setNote(note);
			long ST = DATE.nowAsLong();
			Utils.logInfo(this.getClass(), "ST with " + (ST - GT));
			updateWithEndVariables(transaction, user);
			long UT = DATE.nowAsLong();
			Utils.logInfo(this.getClass(), "UT with " + (UT - ST));

			Transaction_Step activeTransactionStep = transactionStepService.getActive(transaction.getId());
			if (activeTransactionStep != null) {
				activeTransactionStep.setActive(false);
				transactionStepService.update(activeTransactionStep, user);
			}
			long end = DATE.nowAsLong();
			Utils.logInfo(this.getClass(), "End with " + (end - start));
			return true;
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", id_transaction, "Error", "Error finalizando transacción: " + ex.getMessage(), ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @param id_transaction
	 * @param id_transaction_step
	 * @param user
	 * @throws Exception
	 */
	@org.skife.jdbi.v2.sqlobject.Transaction
	public void goBackStep(int id_transaction, int id_transaction_step, User user) throws Exception {
		try {
			V_Transaction transaction = get(id_transaction, user);
			validateRunningTransaction(transaction);
			Transaction_Step transactionStep = transactionStepService.get(id_transaction_step);
			Transaction_Step activeTransactionStep = transactionStepService.getActive(transaction.getId());

			activeTransactionStep.setActive(false);
			transactionStepService.update(activeTransactionStep, user);

			int idNextStep = transactionStep.getId_workflow_step();
			Workflow_Step nextStep = Workflow_Step.getStep(idNextStep, httpClient, config.getApiURL(), user.getToken());
			boolean nextStepIsWF = nextStep.getId_type() == WorkflowStepTypes.WORKFLOW.getValue();

			Transaction_Step nextTransactionStep = transactionStepService.create(
					new Transaction_Step(0,
							transaction.getId_center(),
							transaction.getId(),
							transactionStep.getId_parent(),
							transactionStep.getId_workflow(),
							transactionStep.getId_workflow_step(),
							user.getId(),
							new Date(),
							!nextStepIsWF,
							null
					),
					user
			);
			transaction.setId_current_step(nextStep.getId());
			if (nextStepIsWF) {
				setSubNextStep(transaction, nextStep, nextTransactionStep, user);
			}
			updateWithStepVariables(transaction, user);
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", id_transaction, "Error",
					"Error volviendo a paso anterior #" + id_transaction_step + ": " + ex.getMessage(), ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	private void setSubNextStep(V_Transaction transaction, Workflow_Step stepWorkflow, Transaction_Step parentTransactionStep, User user) throws Exception {
		Workflow nextWorkflow = Workflow.getActive(stepWorkflow.getId_workflow_external(), httpClient, config.getApiURL(), user.getToken());
		Workflow_Step subNextStep = Workflow_Step.getFirstStep(nextWorkflow.getVersion_id(), httpClient, config.getApiURL(), user.getToken());
		boolean nextStepIsWF = subNextStep.getId_type() == WorkflowStepTypes.WORKFLOW.getValue();

		Transaction_Step transactionStep = transactionStepService.create(
				new Transaction_Step(0,
						transaction.getId_center(),
						transaction.getId(),
						parentTransactionStep.getId(),
						subNextStep.getId_workflow(),
						subNextStep.getId(),
						user.getId(),
						new Date(),
						!nextStepIsWF,
						null
				), user
		);
		transaction.setId_current_step(subNextStep.getId());
		if (nextStepIsWF) {
			setSubNextStep(transaction, subNextStep, transactionStep, user);
		}
	}

	/**
	 *
	 * @param id
	 * @param value
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<V_Transaction> changeCurrentStep(final int id, String value, User user) throws Exception {
		try {
			V_Transaction transaction = get(id, user);
			validateRunningTransaction(transaction);
			Workflow_Step currentStep = Workflow_Step.getStepWithOptions(transaction.getId_current_step(), httpClient, config.getApiURL(), user.getToken());
			List<Workflow_Step_Option> options = currentStep.getWorkflow_step_option();
			Workflow_Step_Option selectedOption = null;

			Transaction_Step activeTransactionStep = transactionStepService.getActive(transaction.getId());
			for (int i = 0; i < options.size(); i++) {
				Workflow_Step_Option option = options.get(i);
				boolean matched;
				if (currentStep.getId_type() == WorkflowStepTypes.REGULAR.getValue()) {
					matched = (value == null && option.getValue() == null)
							|| (option.getValue() != null && option.getValue().equals(value));
				} else {
					matched = (value == null && option.getName() == null)
							|| (option.getName() != null && option.getName().equals(value));
				}
				if (matched) {
					selectedOption = option;
					break;
				}
			}
			if (Objects.isNull(selectedOption)) {
				throw new WebApplicationException("Ninguna opción corresponde a este valor", HttpStatus.INTERNAL_SERVER_ERROR_500);
			}
			if (selectedOption.getId_type() == WorkflowStepOptionTypes.STEP) {
				int idNextStep = Integer.parseInt(selectedOption.getValue());
				Workflow_Step nextStep = Workflow_Step.getStep(idNextStep, httpClient, config.getApiURL(), user.getToken());
				boolean nextStepIsWF = nextStep.getId_type() == WorkflowStepTypes.WORKFLOW.getValue();

				activeTransactionStep.setValue(value);
				activeTransactionStep.setActive(false);
				transactionStepService.update(activeTransactionStep, user);

				Transaction_Step nextTransactionStep = transactionStepService.create(
						new Transaction_Step(0,
								transaction.getId_center(),
								transaction.getId(),
								activeTransactionStep.getId_parent(),
								nextStep.getId_workflow(),
								nextStep.getId(),
								user.getId(),
								new Date(),
								!nextStepIsWF,
								null
						), user
				);
				transaction.setId_current_step(nextStep.getId());
				if (nextStepIsWF) {
					setSubNextStep(transaction, nextStep, nextTransactionStep, user);
				}
				updateWithStepVariables(transaction, user);
			} else if (selectedOption.getId_type() == WorkflowStepOptionTypes.VALUE) {
				if (activeTransactionStep.getId_parent() == 0) {
					//Es el flujo primario que está terminando
					activeTransactionStep.setActive(false);
					activeTransactionStep.setValue(selectedOption.getValue());
					transactionStepService.update(activeTransactionStep, user);

					//Si no se limpia id_current_step se podrá saber en qué paso finalizó la transacción
					transaction.setState(TransactionStates.COMPLETED.getValue());
					transaction.setEnd_cause(TransactionEndCauses.END.getValue());
					transaction.setEnd_date(new Date());
					transaction.setEnd_id_employee(user.getId());
					transaction.setEnd_employee_name(user.getName());

					updateWithEndVariables(transaction, user);
				} else {
					//Un flujo secundario está devolviendo valor
					Transaction_Step parentTransactionStep = transactionStepService.get(activeTransactionStep.getId_parent());
					List<Workflow_Step_Option> parentOptions = Workflow_Step_Option.getOptionsByStepID(
							parentTransactionStep.getId_workflow_step(),
							httpClient,
							config.getApiURL(),
							user.getToken());

					Workflow_Step_Option parentSelectedOption = null;
					for (int i = 0; i < parentOptions.size(); i++) {
						Workflow_Step_Option option = parentOptions.get(i);
						// TODO: ¿Debe ir el mismo tipo de match que para selectedOption?
						boolean matched = Utils.stringAreEquals(selectedOption.getValue(), option.getName());
						if (matched) {
							parentSelectedOption = option;
							break;
						}
					}
					if (Objects.isNull(parentSelectedOption)) {
						throw new WebApplicationException("Ninguna opción del paso invocador corresponde a este valor");
					}

					activeTransactionStep.setValue(value);
					activeTransactionStep.setActive(false);
					transactionStepService.update(activeTransactionStep, user);

					parentTransactionStep.setActive(true);
					transactionStepService.update(parentTransactionStep, user);

					transaction.setId_current_step(parentTransactionStep.getId_workflow_step());
					updateWithStepVariables(transaction, user);
					//return changeCurrentStep(id, parentSelectedOption.getValue(), id_empleado);
					return changeCurrentStep(id, parentSelectedOption.getName(), user);
				}
			}
			return getRunningData(id, user);
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", id, "Error", "Error cambiando de paso: " + value, ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<V_Transaction> getRunningData(final int id, User user) throws Exception {
		try {
			V_Transaction transaction = get(id, true, true, user);
			boolean inProgress = transaction.getState() == TransactionStates.RUNNING.getValue();
			Workflow_Step step = Workflow_Step.getStep(transaction.getId_current_step(), true, httpClient, config.getApiURL(), user.getToken());
			transaction.setWorkflow_step(step);
			interpolateContents(transaction);
			if (inProgress && user.getId() == transaction.getId_employee()) {
				if (step.getId_type() == WorkflowStepTypes.EXTERNAL.getValue()) {
					TransactionInterfacesPayload payload = new TransactionInterfacesPayload(transaction, step, "{}");
					TransactionInterfacesResponse response = TransactionInterfacesResponse
							.getInterfaceResponse(payload, httpClient, config.getApiURL(), user.getToken());
					return handleInterfaceResponse(response, transaction, step, user);
				}
			}
			return Arrays.asList(transaction);
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", id, "Error", "Error obteniendo el estado de la transacción: " + ex.getMessage(), ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	void interpolateContents(V_Transaction transaction) {
		if (transaction == null
				|| transaction.getWorkflow_step() == null
				|| Utils.listIsNullOrEmpty(transaction.getWorkflow_step().getWorkflow_step_content())
				|| Utils.listIsNullOrEmpty(transaction.getTransaction_results())) {
			return;
		}
		Workflow_Step step = transaction.getWorkflow_step();
		List<Transaction_Result> transactionResults = transaction.getTransaction_results();

		Map<String, Object> values = new HashMap();
		for (Transaction_Result result : transactionResults) {
			values.put(result.getName(), Utils.coalesce(result.getValue(), Transaction_Result.NULL_VALUE));
		}
		values.put("transaction", transaction);
		values.put("transactionResults", transactionResults);
		values.put("step", step);

		step.getWorkflow_step_content().forEach(stepContent -> {
			Content content = stepContent.getContent();
			if (content != null) {
				try {
					values.put("content", content);
					String template = content.getValue();
					String interpolated = Velocity.interpolate(template, values);
					content.setValue(interpolated);
				} catch (Exception ex) {
					Utils.logException(this.getClass(), null, ex);
				}
			}
		});

	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<V_Transaction> getCreateData(User user) throws Exception {
		try {
			V_Transaction transaction = new V_Transaction();
			transaction.setStart_date(Date.from(Instant.now()));
			Workflow_Step step = new Workflow_Step();
			step.setWorkflow_step_option(new ArrayList<>());

			Workflow_Step_Content workflowStepContent = new Workflow_Step_Content(
					-1, -1, -1, -1, -1, ID_GREETING_CONTENT, -1
			);
			Content content = Content.getContent(workflowStepContent.getId_content(), httpClient, config.getApiURL(), user.getToken());
			workflowStepContent.setContent(content);
			List<Workflow_Step_Content> workflowStepContents = new ArrayList<>();
			workflowStepContents.add(workflowStepContent);

			step.setWorkflow_step_content(workflowStepContents);
			transaction.setWorkflow_step(step);

			return Arrays.asList(transaction);
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", 0, "Error", "Error obteniendo información de creación de la transacción", ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @param transaction
	 * @param results
	 * @param user
	 */
	public void saveResults(V_Transaction transaction, List<Transaction_Result> results, User user) {
		saveResults(transaction, results, false, user);
	}

	/**
	 *
	 * @param transaction
	 * @param results
	 * @param createOnly
	 * @param user
	 */
	public void saveResults(V_Transaction transaction, List<Transaction_Result> results, boolean createOnly, User user) {
		if (results != null) {
			results.forEach(result -> {
				boolean canSave = Transaction_Result.canSave(result);
				InterfaceConfigs.log(this.getClass(), "canSave result " + result.getName() + " " + canSave);
				if (canSave) {
					result.setId_center(transaction.getId_center());
					result.setId_transaction(transaction.getId());
					result.setId_workflow_step(transaction.getId_current_step());
					if (createOnly) {
						transactionResultService.createOnly(result, user);
					} else {
						transactionResultService.createOrUpdate(result, user);
					}
				}
			});
		}
	}

	/**
	 *
	 * @param params
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<V_Transaction> sendToInterface(final IdValueParams params, User user) throws Exception {
		try {
			String payloadForm = params.getValue();
			V_Transaction transaction = get(params.getId(), true, true, user);
			validateRunningTransaction(transaction);
			Workflow_Step step = Workflow_Step.getStep(transaction.getId_current_step(), true, httpClient, config.getApiURL(), user.getToken());
			TransactionInterfacesPayload payload = new TransactionInterfacesPayload(transaction, step, payloadForm);

			TransactionInterfacesResponse response = TransactionInterfacesResponse.getInterfaceResponse(payload, httpClient, config.getApiURL(), user.getToken());
			return handleInterfaceResponse(response, transaction, step, user);
		} catch (Exception ex) {
			log().insert(user.getId(), "Transacciones", params.getId(), "Error", "Error consultando interfaz en el paso: " + ex.getMessage(), ex, user.getIp());
			throw new WebApplicationException(ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR_500);
		}
	}

	/**
	 *
	 * @param response
	 * @param transaction
	 * @param step
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<V_Transaction> handleInterfaceResponse(TransactionInterfacesResponse response, V_Transaction transaction, Workflow_Step step, User user) throws Exception {
		transaction.setWorkflow_step(step);
		boolean updated = handleTransactionChanges(response, transaction, user);
		InterfaceConfigs.log(this.getClass(), "Interface response: ");
		InterfaceConfigs.log(this.getClass(), JSON.toString(response));
		Interface inter = response.getInterface();
		if (inter != null && !inter.isKeep_results() && inter.getId() != Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE) {
			transactionResultService.delete(transaction.getId(), inter.getId());
		}
		if (Utils.listNonNullOrEmpty(response.getLogs())) {
			response.getLogs().forEach(log -> {
				if (log != null) {
					log().insert(log);
				}
			});
		}

		List<Transaction_Result> interfaceResults = response.getResults();
		boolean createOnly = inter == null ? true : !inter.isKeep_results() && inter.getId() != Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE;
		saveResults(transaction, interfaceResults, createOnly, user);
		if (updated) {
			V_Transaction tempTransaction = updateTransactionVariables(transaction.getId(), user);
			transaction = copyChildren(tempTransaction, transaction);
		}

		switch (response.getAction()) {
			case NEXT_STEP:
				// TODO: Evaluar pasarle el payload a changeCurrentStep para evitar repetir consultas
				return changeCurrentStep(transaction.getId(), response.getValue(), user);
			case SHOW_FORM:
				step.setInterface(response.getInterface());
				break;
			case INVALID_REQUEST:
				// TODO: Notificar en la pantalla cuáles campos no se encontraron o fueron inválidos
				step.setInterface(response.getInterface());
				break;
			case ERROR:
				// TODO: Notificar en la pantalla del representante que hubo un error y evitar continuar.
				//step.setInterface(response.getInterface());
//				transaction.setWorkflow_step(null);
				return changeCurrentStep(transaction.getId(), response.getValue(), user);
			default:
				break;
		}
		return Arrays.asList(transaction);
	}

	/**
	 *
	 * @param response
	 * @param transaction
	 * @param user
	 * @return
	 */
	public boolean handleTransactionChanges(TransactionInterfacesResponse response, V_Transaction transaction, User user) {
		if (Utils.isNullOrEmpty(response.getTransactionChanges())) {
			return false;
		}
		boolean saveChanges = false;
		for (Map.Entry<String, String> prop : response.getTransactionChanges().entrySet()) {
			String key = prop.getKey();
			String value = prop.getValue();
			switch (key) {
				case "id_category":
					int id_category = Integer.parseInt(value);
					Category category = categoryService.getOnly(id_category);
					if (category != null) {
						List<Transaction_Result> results = new ArrayList();
						if (null != category.getId_type()) {
							switch (category.getId_type()) {
								case CategoryTypes.TYPE1: {
									transaction.setId_category(category.getId());
									Transaction_Result categoryIdResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE1_ID, CategoryTypes.TYPE1_ID_LABEL,
											String.valueOf(category.getId()), ""
									);
									results.add(categoryIdResult);
									Transaction_Result categoryNameResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE1_NAME, CategoryTypes.TYPE1_NAME_LABEL,
											category.getName(), ""
									);
									results.add(categoryNameResult);
									break;
								}
								case CategoryTypes.TYPE2: {
									transaction.setId_category(category.getId());
									Transaction_Result categoryIdResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE2_ID, CategoryTypes.TYPE2_ID_LABEL,
											String.valueOf(category.getId()), ""
									);
									results.add(categoryIdResult);
									Transaction_Result categoryNameResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE2_NAME, CategoryTypes.TYPE2_NAME_LABEL,
											category.getName(), ""
									);
									results.add(categoryNameResult);
									break;
								}
								case CategoryTypes.TYPE3: {
									transaction.setId_category(category.getId());
									Transaction_Result categoryIdResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE3_ID, CategoryTypes.TYPE3_ID_LABEL,
											String.valueOf(category.getId()), ""
									);
									results.add(categoryIdResult);
									Transaction_Result categoryNameResult = new Transaction_Result(
											0, Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.TYPE3_NAME, CategoryTypes.TYPE3_NAME_LABEL,
											category.getName(), ""
									);
									results.add(categoryNameResult);
									break;
								}
								case CategoryTypes.ACTION:
									int steps = Utils.coalesce(transaction.getTransaction_steps(), new ArrayList()).size();
									Transaction_Result categoryResult = new Transaction_Result(
											0,
											Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE,
											CategoryTypes.PREFIX + CategoryTypes.ACTION + category.getId() + "_" + steps,
											CategoryTypes.ACTION_LABEL + " " + steps,
											category.getName(), ""
									);
									results.add(categoryResult);
									break;
								default:
									break;
							}
							if (!results.isEmpty()) {
								saveChanges = true;
								saveResults(transaction, results, user);
							}
						}
					}
					break;
				default:
					break;
			}
		}
		if (saveChanges) {
			dao().update(transaction);
			log().insert(user.getId(), "Transacciones", transaction.getId(), "Actualizar", transaction, null, user.getIp());
		}
		return saveChanges;
	}

	/**
	 *
	 * @param destination
	 * @param source
	 * @return
	 */
	public V_Transaction copyChildren(V_Transaction destination, V_Transaction source) {
		destination.setTransaction_results(source.getTransaction_results());
		destination.setTransaction_steps(source.getTransaction_steps());
		destination.setWorkflow_step(source.getWorkflow_step());
		return destination;
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
