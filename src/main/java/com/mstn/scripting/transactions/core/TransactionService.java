/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Transaction;
import com.mstn.scripting.transactions.ScriptingTransactionsConfiguration;
import com.mstn.scripting.transactions.db.TransactionDao;
import com.mstn.scripting.transactions.db.TransactionStepDao;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.apache.http.client.HttpClient;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con consultas de transacciones.
 *
 * @author amatos
 */
public abstract class TransactionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	private final int ID_GREETING_CONTENT = -1;

	private ScriptingTransactionsConfiguration config;
	private HttpClient httpClient;
	private TransactionStepService transactionStepService;
	private V_TransactionStepService vTransactionStepService;
	private TransactionResultService transactionResultService;

	/**
	 *
	 */
	public TransactionService() {
	}

	/**
	 *
	 * @return
	 */
	public ScriptingTransactionsConfiguration getConfig() {
		return config;
	}

	/**
	 *
	 * @param config
	 * @return
	 */
	public TransactionService setConfig(ScriptingTransactionsConfiguration config) {
		this.config = config;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 * @return
	 */
	public TransactionService setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public TransactionStepService getTransactionStepService() {
		return transactionStepService;
	}

	/**
	 *
	 * @param transactionStepService
	 * @return
	 */
	public TransactionService setTransactionStepService(TransactionStepService transactionStepService) {
		this.transactionStepService = transactionStepService;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public V_TransactionStepService getV_TransactionStepService() {
		return vTransactionStepService;
	}

	/**
	 *
	 * @param vTransactionStepService
	 * @return
	 */
	public TransactionService setV_TransactionStepService(V_TransactionStepService vTransactionStepService) {
		this.vTransactionStepService = vTransactionStepService;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public TransactionResultService getTransactionResultService() {
		return transactionResultService;
	}

	/**
	 *
	 * @param transactionResultService
	 * @return
	 */
	public TransactionService setTransactionResultService(TransactionResultService transactionResultService) {
		this.transactionResultService = transactionResultService;
		return this;
	}

	@CreateSqlObject
	abstract TransactionDao dao();

	@CreateSqlObject
	abstract TransactionStepDao transactionStepDao();

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Transaction> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Transaction> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id_customer
	 * @return
	 */
	public List<Transaction> getByCustomer(String id_customer) {
		return dao().getByCustomer(id_customer);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Transaction get(int id) {
		Transaction item = dao().get(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 *
	 * @param id
	 * @param loadResults
	 * @param loadSteps
	 * @return
	 */
	public Transaction get(int id, boolean loadResults, boolean loadSteps) {
		Transaction item = get(id);
		if (loadResults) {
			item.setTransaction_results(
					transactionResultService.getAllByTransaction(item.getId())
			);
		}
		if (loadSteps) {
			item.setTransaction_steps(
					vTransactionStepService.getAllByTransaction(item.getId())
			);
		}
		return item;
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
