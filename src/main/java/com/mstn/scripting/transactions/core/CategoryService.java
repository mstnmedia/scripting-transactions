/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.transactions.db.CategoryDao;
import com.mstn.scripting.core.models.Category;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con las categorías.
 *
 * @author amatos
 */
public abstract class CategoryService {

	private static final String NOT_FOUND = "No se encontró el registro id %s.";
	private static final String HAS_DEPENDENTS = "El registro id %s tiene dependientes. No puedes eliminarlo.";
	private static final String CANT_CHANGE_TYPE = "No puedes cambiar el tipo de categoría. Este registro tiene dependientes.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public CategoryService() {

	}

	@CreateSqlObject
	abstract CategoryDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Category>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Category> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Category> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Category> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Category getOnly(int id) {
		Category item = dao().get(id);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Category get(int id) {
		Category item = getOnly(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Category create(Category item, User user) {
		if (!user.isMaster()) {
			item.setId_center(user.getCenterId());
		}
		int id = dao().insert(item);
		Category after = get(id);
		log().insert(user.getId(), "Categorías", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Category update(Category item, User user) {
		Category before = get(item.getId());
		if (!Utils.equals(before.getId_type(), item.getId_type())) {
			long childrenNo = getCount(new WhereClause(
					new Where("id_parent", item.getId()))
			);
			if (childrenNo > 0) {
				throw new WebApplicationException(String.format(CANT_CHANGE_TYPE, item.getId()), Status.BAD_REQUEST);
			}
		}
		if (!user.isMaster()) {
			item.setId_center(before.getId_center());
		}
		dao().update(item);
		Category after = get(item.getId());
		log().insert(user.getId(), "Categorías", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Category before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Categorías", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
