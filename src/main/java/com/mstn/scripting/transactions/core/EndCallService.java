/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.EndCall;
import com.mstn.scripting.transactions.db.EndCallDao;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con las razones de fines de llamadas.
 *
 * @author amatos
 */
public abstract class EndCallService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String HAS_DEPENDENTS = "item id %s has dependents. You can't delete it now.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 *
	 */
	public EndCallService() {

	}

	@CreateSqlObject
	abstract EndCallDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param where
	 * @return
	 * @throws Exception
	 */
	public WhereClause getUserWhere(User user, String where) throws Exception {
		List<Where> clientWhere = Arrays.asList(JSON.toObject(where, Where[].class));
		return new WhereClause(clientWhere);
//		Where userWheres = new Where("id_center", user.getCenterId());
//		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Representation<List<EndCall>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy, User user) throws Exception {
		WhereClause whereClause = getUserWhere(user, where);
		long totalRows = getCount(whereClause);
		List<EndCall> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<EndCall> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<EndCall> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public EndCall getOnly(int id) {
		EndCall item = dao().get(id);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public EndCall get(int id) {
		EndCall item = getOnly(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public EndCall create(EndCall item, User user) {
		int id = dao().insert(item);
		EndCall after = get(id);
		log().insert(user.getId(), "Razones de fin de llamada", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	@Transaction
	public EndCall update(EndCall item, User user) {
		EndCall before = get(item.getId());
		dao().update(item);
		EndCall after = get(item.getId());
		log().insert(user.getId(), "Razones de fin de llamada", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	@Transaction
	public String delete(final int id, User user) {
		EndCall before = get(id);
		boolean hasDependents = dao().hasDependents(id);
		int result = hasDependents ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Razones de fin de llamada", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			case -1:
				throw new WebApplicationException(String.format(HAS_DEPENDENTS, id), Status.PRECONDITION_FAILED);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
