/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.transactions.core;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.models.InterfaceConfigs;
import com.mstn.scripting.transactions.ScriptingTransactionsApplication;
import com.mstn.scripting.transactions.db.dashboard.DashboardRepresentation;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase que permite consultar el procedimiento del cálculo de NPS de la cadena
 * de mando del usuario indicado.
 *
 * @author amatos
 */
public class DashboardNPSService {

	/**
	 *
	 */
	public DashboardNPSService() {

	}

	List<NPSItem> callProcedure(User user, String from, String to) throws SQLException {
		HashMap<Integer, Employee> mapEmployees = new HashMap();
		List<Employee> employees = ScriptingTransactionsApplication.EmployeeService
				.getEmployeeTreeDown(user.getId());
		List<Integer> delegating = ScriptingTransactionsApplication.EmployeeService
				.getDelegationService()
				.getByDelegateID(user.getId())
				.stream()
				.map(item -> item.getId_user())
				.collect(Collectors.toList());
		//String treeDownStr = "select tarjeta_empleado, tarjeta_supervisor from servicio.encuesta_cadena_de_mando (nolock) where codigo_segmento = 6 and tarjeta_supervisor <> '0' and estatus_empleado = 1;)"
		String treeDownStr = "select distinct * from (values "
				+ String.join(",", employees.stream()
						.map(e -> {
							mapEmployees.put(e.getId(), e);
							int cima = e.getId() == user.getId() || delegating.contains(e.getId()) ? 1 : 0;
							return "(" + e.getId() + ", " + e.getId_parent() + ", " + cima + ")";
						})
						.collect(Collectors.toList())
				) + ") as tD(tarjeta_empleado, tarjeta_supervisor, cima)";
		String query = ""
				+ "declare @from date = cast('" + from + "' as date);"
				+ "declare @to date = cast('" + to + "' as date);"
				+ "declare @treeDown as servicio.tblEmpleado;\n"
				+ "insert @treeDown " + treeDownStr + ";\n"
				+ "exec [servicio].[resultado_nps] @from, @to, @treeDown";

		String connectionString = InterfaceConfigs.get("npsConnectionString");
		Connection con = DriverManager.getConnection(connectionString);
		CallableStatement cs = con.prepareCall(query);
		ResultSet resultSet = cs.executeQuery();

		HashMap<String, Boolean> hasColumn = new HashMap<>();
		ResultSetMetaData rsmd = resultSet.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
		}

		HashMap<Integer, List<NPSItem>> mapNPS = new HashMap();
		while (resultSet.next()) {
			int tarjeta_empleado = hasColumn.containsKey(TARJETA_EMPLEADO) ? resultSet.getInt(TARJETA_EMPLEADO) : 0;
			if (!mapNPS.containsKey(tarjeta_empleado)) {
				mapNPS.put(tarjeta_empleado, new ArrayList());
			}
			NPSItem item = new NPSItem(
					hasColumn.containsKey(CANAL) ? resultSet.getString(CANAL) : "",
					tarjeta_empleado,
					hasColumn.containsKey(TARJETA_SUPERVISOR) ? resultSet.getInt(TARJETA_SUPERVISOR) : 0,
					mapEmployees.getOrDefault(tarjeta_empleado, new Employee()).getName(),
					hasColumn.containsKey(NPS) ? resultSet.getDouble(NPS) : 0.0
			);
			mapNPS.get(tarjeta_empleado).add(item);
		}
		List<NPSItem> response = new ArrayList();
		for (int i = 0; i < employees.size(); i++) {
			Employee e = employees.get(i);
			if (e.getId() == user.getId()
					|| e.getId_parent() == user.getId()
					|| delegating.contains(e.getId())
					|| delegating.contains(e.getId_parent())) {
				List<NPSItem> employeeNPS = mapNPS.getOrDefault(e.getId(), Arrays.asList(
						new NPSItem("", e.getId(), e.getId_parent(), e.getName(), 0)
				));
				response.addAll(employeeNPS);
			}
		}
		return response;
	}

	/**
	 *
	 * @param user
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	public DashboardRepresentation resultadoNPS(User user, String from, String to) throws Exception {
		from = DATE.toString(DATE.toDate(from));
		to = DATE.toString(DATE.toDate(to));
		List<NPSItem> items = callProcedure(user, from, to);
		DashboardRepresentation result = new DashboardRepresentation();
		result.setItems(items);
		return result;
	}

	private static final String CANAL = "canal";
	private static final String TARJETA_EMPLEADO = "tarjeta_empleado";
	private static final String TARJETA_SUPERVISOR = "tarjeta_supervisor";
	private static final String NPS = "nps";

	/**
	 *
	 */
	static public class NPSItem {

		/**
		 *
		 */
		public String segmento;

		/**
		 *
		 */
		public String nombre;

		/**
		 *
		 */
		public int tarjeta_empleado;

		/**
		 *
		 */
		public int tarjeta_supervisor;

		/**
		 *
		 */
		public double nps;

		/**
		 *
		 * @param segmento
		 * @param tarjeta_empleado
		 * @param tarjeta_supervisor
		 * @param nombre
		 * @param nps
		 */
		public NPSItem(String segmento, int tarjeta_empleado, int tarjeta_supervisor, String nombre, double nps) {
			this.segmento = segmento;
			this.tarjeta_empleado = tarjeta_empleado;
			this.tarjeta_supervisor = tarjeta_supervisor;
			this.nombre = nombre;
			this.nps = nps;
		}

	}
}
